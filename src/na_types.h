#ifndef na_types_H
#define na_types_H

//-----------------------------------------------------------------------------
// Fortran function calling macros (with and without underscore)
#ifdef GNU_COMPILER
#define FSUB(a) a ## _
#endif

#ifdef IBM_XL_COMPILER
#define FSUB(a) a
#endif

//-----------------------------------------------------------------------------
#include "RngStream.h"
//-----------------------------------------------------------------------------
typedef struct{	
	double *ranges_in;
	double *scales_in;
	RngStream *prngstream;
	int nd;
	int nsamplei;
    int nsamplei_file;
	int nsample;
	int restart;
	int manadvance;
	int ncells;  // number of cells to resample
	int itmax;   // Maximum number of iterations
	int nummodels;
	int restartna;
	int nainit_idnext;
	int opt_rank,opt_size;
	int initsamp;
	int numwbuff;
	int numdisc;
} NaUserContext;

typedef struct{
	double mfit_minc,mfit_min,mfit_mean;
	int    mfit_opt,*ff_mfit_ord;
	double *work;
	int    *ff_ind,*ff_iwork;
} NaMfitStatistics;

typedef struct{
	int       cntModels_own,cntModels,tag;
	double   *sca_model,*current_model;
	double    current_misfit;
	int       recv_buffer_size;
	double   *recv_buffer,diff_t;
	int       InitOrMain;
} NaCurrentResult;

typedef struct{
	int       ind,size;
	double   *ibuff;
} NaWriteBuffer;

//-----------------------------------------------------------------------------
// FORTRAN 2D array access pattern
//a(row,col) = a[col*num_rows + row]
#define a(i, j) a[(j)*m + (i)]
#define ranges_in(i, j) ranges_in[(j)*2 + (i)]
#define ranget(i, j) ranget[(j)*2 + (i)]
//-----------------------------------------------------------------------------
// Big or little endian system
#define IS_LITTLE_ENDIAN ( (*(char*)&num) == 1 )



#endif
