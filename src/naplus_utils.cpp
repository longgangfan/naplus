#include "naplus_utils.h"
//-----------------------------------------------------------------------------
void communicate_task(MPI_Comm comm,int *task){
    MPI_Bcast(task,1,MPI_INTEGER,0,comm);
}
//-----------------------------------------------------------------------------
void check_commandline(MPI_Comm comm, int argc,char *argv[],int *fwrd_group_size,int *fwrd_group_num)
{
    int world_rank,world_size;

    MPI_Comm_rank(comm, &world_rank);
    MPI_Comm_size (comm, &world_size);

    if(argc<2)
    {
        printf("[NAplus:  all] Not enough command line arguments\n");
        MPI_Abort(comm,MPI_ERR_UNKNOWN);
    }
    else if (argc==2)
    {
        if(strcmp(argv[1],"-help") == 0)
        {
            if(world_rank==0) printf("[NAplus Help: all] Invoke NAplus as follows: \n");
            if(world_rank==0) printf("[NAplus Help: all] <mpirun-command & options> NAplus -groupsize <number of CPUs used per forward model> \n");
            MPI_Finalize();
            exit(EXIT_SUCCESS);
        }
        else
        {
            if(world_rank==0) printf("[NAplus Error: all] Unknown Command line argument: %s\n",argv[1]);
            MPI_Abort(comm,MPI_ERR_UNKNOWN);
        }
    }
    else
    {
        if(strcmp(argv[1],"-groupsize") == 0)
        {
            *fwrd_group_size = atoi(argv[2]);
            *fwrd_group_num  = world_size/(*fwrd_group_size);
            if(world_size%(*fwrd_group_size) != 0)
            {
                printf("[NAplus Error: all]  Groupsize does not fit to the given number of processors\n");
                MPI_Abort(comm,MPI_ERR_UNKNOWN);
            }
            if(world_rank==0) printf("Groupsize:          %s\n",argv[2]);
            if(world_rank==0) printf("Number of groups:   %d\n",*fwrd_group_num);
            MPI_Barrier(comm);
        }
        else
        {
            if(world_rank==0) printf("[NAplus Error: all] Unknown Command line argument: %s, %s\n",argv[1],argv[2]);
            MPI_Abort(comm,MPI_ERR_UNKNOWN);
        }
    }
    return;
}
//-----------------------------------------------------------------------------
void load_inputfile(MPI_Comm comm)
{
    FILE    *ifp;
    int      found;
    int      nd;
    char    *DensIdx_char,*ViscIdx_char;
    int      testvar;
    int      rank;

    MPI_Comm_rank(comm, &rank);

    if(!rank){
        printf("--------------------------------------------------------------------------------\n");
        printf("Global NAplus struct:\n");
        printf("=====================\n");
    }

    // open na.in
    ifp = fopen("na.in", "r");
    if (ifp == NULL) {
        fprintf(stderr, "[NAplus Error: all] Can't open input file na.in \n");
        exit(1);
    }

    parse_GetInt(ifp,"FwrdModel"  ,&(naplus.FwrdModel), &found );
    parse_GetInt(ifp,"ParamDim"  ,&nd, &found );
    parse_GetInt(ifp,"DensN"     ,&(naplus.DensN), &found );
    parse_GetInt(ifp,"ViscN"     ,&(naplus.ViscN), &found );
    parse_GetDouble(ifp,"DensRef",&(naplus.DensRef), &found );

    if(!rank) printf("%d density parameter, %d viscosity parameter \n",naplus.DensN,naplus.ViscN);
    if(!rank) printf("Reference density: %g \n",naplus.DensRef);

    if((naplus.DensN+naplus.ViscN) != nd){
    printf("[NAplus Error] DensN + ViscN != nd \n");
        MPI_Abort(comm,MPI_ERR_UNKNOWN);
    }

    // allocate array for indices of densities
    naplus.DensIdx = new int[naplus.DensN];
    for(int k=0;k<naplus.DensN;k++){
        asprintf(&DensIdx_char,"DensIdx[%d]",k);
        parse_GetInt(ifp,DensIdx_char, &testvar, &found );
        naplus.DensIdx[k] = testvar;
        if(!rank) printf("%s = %d\n",DensIdx_char,testvar);
        free(DensIdx_char);
    }

    // allocate array for indices of viscosities
    naplus.ViscIdx = new int[naplus.ViscN];
    for(int k=0;k<naplus.ViscN;k++){
        asprintf(&ViscIdx_char,"ViscIdx[%d]",k);
        parse_GetInt(ifp,ViscIdx_char, &testvar, &found );
        naplus.ViscIdx[k] = testvar;
        if(!rank) printf("%s = %d\n",ViscIdx_char,testvar);
        free(ViscIdx_char);
    }

    fclose(ifp);
    return;
}


//-----------------------------------------------------------------------------
void MPI_Comm_split_host(MPI_Comm comm_in, MPI_Comm *comm_host)
{
	int       rank,nprocs, len;
	const int ProcNameLen=40;
	char      host_name[ProcNameLen];
	char     *host_names;
	int       cnt=0, cntcolor=1;
	int      *color;

	// mpi essentials
	MPI_Comm_size(comm_in,&nprocs);
	MPI_Comm_rank(comm_in,&rank);

    // get hostnames
	MPI_Get_processor_name(host_name, &len);
    if (len>ProcNameLen)
	{
		printf("[NAplus:  all] hostname might be a bit wired :%d > %d\n",len,ProcNameLen);
		MPI_Abort(comm_in,MPI_ERR_UNKNOWN);
	}	

    // allocate an array for all hostnames
    host_names = new char[ProcNameLen*nprocs];

    // collect hostnames of other cpus
    MPI_Allgather(host_name,ProcNameLen, MPI_CHAR ,host_names,ProcNameLen ,MPI_CHAR,comm_in);

    // allocate array to store color
    color = new int[nprocs];
	zeromem(color,nprocs);

    while(cnt<nprocs)
    {
		if (color[cnt] == 0)
		{
			color[cnt] = cntcolor;
			for (int i=cnt+1;i< nprocs;i++)
			{
				if(strcmp(&host_names[cnt*ProcNameLen],&host_names[i*ProcNameLen]) == 0)
				{
					color[i] = cntcolor;
				}
			}
			cntcolor++;
		}
		cnt++;
	}

    //printf("[%d]  on host  %d  host  %s %d %d\n", rank, color[rank], host_name,ProcNameLen,len);


    // create communicator for hosts - must be destroyed again!
    MPI_Comm_split(comm_in, color[rank], rank, comm_host);

    // clean up
	delete [] host_names;
	delete [] color;

	return;
}
