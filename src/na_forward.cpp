#include "na_forward.h"

//-----------------------------------------------------------------------------
void print_user_info(int opt_rank,int var)
{
    switch (var)
    {
    case 1:
        if(!opt_rank){
            printf("--------------------------------------------------------------------------------\n");
            printf("  mID  |  gID  |  T [s]  | type \n");
            printf("--------------------------------\n");
        }
        break;
    case 2:
            printf("=> Group %d has finished \n",opt_rank+1);
        break;
    case 3:
        if(!opt_rank){
            printf("-------------------------\n");
            printf("Start main optimization \n");
            printf("-------------------------\n");
        }
        break;
    }
    return;
}
//-----------------------------------------------------------------------------
void forward(int nd,NaCurrentResult &cr,MPI_Comm fwrd_comm,int opt_cpu,int fwrd_group_id,int cntModels)
{
    int task, fwrd_size;
    
    // Get groupsize
    MPI_Comm_size(fwrd_comm, &fwrd_size);

    // Communicate to group
    if(fwrd_size>1)
    {
        task=1; communicate_task(fwrd_comm,&task);
    }
    
    if(naplus.FwrdModel==0)
        forward_LaMEM(nd,cr.current_model,&cr.current_misfit,fwrd_comm,opt_cpu,fwrd_group_id,cntModels,cr.InitOrMain,cr.diff_t);
    else if(naplus.FwrdModel==1)
       forward_matlab(nd,cr.current_model,&cr.current_misfit,fwrd_comm,opt_cpu,fwrd_group_id,cntModels,cr.InitOrMain,cr.diff_t);
    else if(naplus.FwrdModel==2)
        Rosenbrock2D(nd,cr.current_model,&cr.current_misfit);
    else if(naplus.FwrdModel==3)
        Himmelsblau2D(nd,cr.current_model,&cr.current_misfit);
    else if(naplus.FwrdModel==4)
        MultipleLocalMinima2D(nd,cr.current_model,&cr.current_misfit);
    else if(naplus.FwrdModel==5)
        RastriginND(nd,cr.current_model,&cr.current_misfit);
    else if(naplus.FwrdModel==6)
        RosenbrockND(nd,cr.current_model,&cr.current_misfit);
    else if(naplus.FwrdModel==7)
    	EffViscosity(cr.current_model, &cr.current_misfit);
    else
    {
        fprintf(stderr, "[NAplus Error: all] This forward model doesn't exist \n");
        MPI_Finalize();
        exit(1);
    }

    // Communicate to group
    if(fwrd_size>1)
    {
    task=0; communicate_task(fwrd_comm,&task);
    }

    return;
}
//-----------------------------------------------------------------------------
#undef __FUNCT__
#define __FUNCT__ "forward_LaMEM"
void forward_LaMEM(int nd,double *current_model,double *misfit,MPI_Comm fwrd_comm,int opt_cpu,int fwrd_group_id,int cntModels,int IorM, double &diff_t)
{
    time_t          start_t,end_t;

    //  --- send and receive forward modeling input data ---

    // Group ID
    MPI_Bcast(&fwrd_group_id,1,MPI_INT,0,fwrd_comm);

    // number of model parameter
    MPI_Bcast(&nd,1,MPI_INT,0,fwrd_comm);
    // model parameter
    if(opt_cpu != 0) current_model = new double[nd];
    MPI_Bcast(current_model,nd,MPI_DOUBLE,0,fwrd_comm);
    // misfit
    MPI_Bcast(misfit,1,MPI_DOUBLE,0,fwrd_comm);
    // model number
    MPI_Bcast(&cntModels,1,MPI_INT,0,fwrd_comm);

    // time stamp
    time(&start_t);
#ifdef LaMEM
    // initialize LaMEM
    LaMEMInterface_Initialize(fwrd_comm,current_model,cntModels,fwrd_group_id,start_t);
    LaMEMLib(misfit,&fwrd_group_id);
    //RosenbrockND(nd,current_model,misfit);
#else
    RosenbrockND(nd,current_model,misfit);
#endif

    // time stamp
    time(&end_t);
    diff_t = difftime(end_t,start_t);

#ifdef LaMEM
    // finalize LaMEM
    LaMEMInterface_Finalize(fwrd_comm,*misfit,cntModels,diff_t);
#endif
    // command line display
    if(opt_cpu == 0){
        if (IorM==0)
            printf(" %5d   %5d   %6.0fs   init\n",cntModels,fwrd_group_id,diff_t);
        else
            printf(" %5d   %5d   %6.0fs   main\n",cntModels,fwrd_group_id,diff_t);
    }

    // deallocate model parameter array
    if(opt_cpu != 0) delete [] current_model;

    return;
}
//-----------------------------------------------------------------------------
#ifdef LaMEM
void LaMEMInterface_Initialize(MPI_Comm fwrd_comm, double *current_model,int cntModels,int fwrd_group_id,time_t start_t)
{
    char              *LaMEM_options,*filename_groupX;
    char              *temp,*densities,*viscosities;
    struct tm         *timeinfo;
    size_t             N=20;
    char               time_now[N];
    static const char *mu_format = "-mu_%d %e"; // 10 characters
    static const char *rho_format = "-rho_%d %G";// 11 characters
    //static const char *mu_format = " -lithdepth_%d %G"; // 10 characters
    //static const char *rho_format = "-lithrho_%d %G";// 11 characters
    string             str;
    double             tempvar;
    int                n;

    // convert time
    timeinfo = localtime(&start_t);
    strftime(time_now,N,"%d-%b-%y %H:%M:%S",timeinfo);

    // redirect stdout to group specific file
    asprintf(&filename_groupX,"LOG_NAplus_%s_Group%d.txt",naplus.fileID,fwrd_group_id);
    freopen (filename_groupX,"a", stdout);
    free(filename_groupX);

    // create viscosities options
    n=0;
    for (int k=0;k<naplus.ViscN;k++){
        tempvar = pow(10,current_model[k]);
        asprintf(&temp,mu_format,naplus.ViscIdx[n],tempvar);
        str += temp;
        str += " ";
        free(temp);
        n++;
    }
    asprintf(&viscosities,str.c_str());

    // clear str
    str.clear();

    // create densities options
    n=0;
    for (int k=naplus.ViscN;k<(naplus.ViscN+naplus.DensN);k++){
        tempvar = naplus.DensRef + current_model[k];
        asprintf(&temp,rho_format,naplus.DensIdx[n],tempvar);
        str += temp;
        str += " ";
        free(temp);
        n++;
    }
    asprintf(&densities,str.c_str());

    // insert options
    asprintf(&LaMEM_options,"-ParamFile SaltDiapirs_Org_GetOpt.dat %s %s -SetMaterialProperties",densities,viscosities);
    //asprintf(&LaMEM_options,"-ParamFile SaltDiapirs_Org_GetOpt.dat %s %s -SetLithColumn",densities,viscosities);
    PetscOptionsInsertString(LaMEM_options);

    // print info
    PetscPrintf(fwrd_comm,"\n\n");
    PetscPrintf(fwrd_comm,"*******************************************************************************************************************\n");
    PetscPrintf(fwrd_comm,"   > time      : %s\n",time_now);
    PetscPrintf(fwrd_comm,"   > start model no. : %d\n",cntModels);
    PetscPrintf(fwrd_comm,"   > group id. : %d\n",fwrd_group_id);
    PetscPrintf(fwrd_comm,"   > parameters: %s\n",densities);
    PetscPrintf(fwrd_comm,"   >           : %s\n\n",viscosities);

    // free oprtions
    free(LaMEM_options);
    free(viscosities);
    free(densities);

    return;
}
#endif
//-----------------------------------------------------------------------------
#ifdef LaMEM
void LaMEMInterface_Finalize(MPI_Comm fwrd_comm,double misfit, int cntModels,double diff_t)
{
    string     filename_stdout,strfileID;

    // print info
    PetscPrintf(fwrd_comm,"\n");
    PetscPrintf(fwrd_comm,"   > finished model no. : %d\n",cntModels);
    PetscPrintf(fwrd_comm,"   > misfit    : %G\n",misfit);
    PetscPrintf(fwrd_comm,"   > duration  : %G sec\n",diff_t);
    PetscPrintf(fwrd_comm,"*******************************************************************************************************************\n");

#ifdef REDIRECT_STDOUT
    // redirect stdout to stdout-file
    strfileID=naplus.fileID;
    filename_stdout = "STDOUT_NAplus_" + strfileID + ".txt";
    freopen (filename_stdout.c_str(),"a", stdout);
#else
    // redirect stdout to screen
    freopen("/dev/tty", "w",stdout);
#endif

    return;
}
#endif
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
/*
 * from: http://en.wikipedia.org/wiki/Rosenbrock_function
 * global minima at
 * f(1,1) = 0
 *
 */
void Rosenbrock2D(int nd, double *current_model, double *misfit)
{
    double x,y,funcval,minval;

    x = current_model[0];
    y = current_model[1];
    funcval = pow((1.0-x),2) + 100.0*pow((y-x*x),2);
    minval = 0.0;

    // misfit
    *misfit = sqrt( (funcval-minval)*(funcval-minval) );

    return;
}
//-----------------------------------------------------------------------------
/*
 * from: http://en.wikipedia.org/wiki/Himmelblau%27s_function
 *
 * local minima at
 * f(3,2) = 0.0
 * f(-2.805118,3.131312) = 0.0
 * f(-3-779310,-3.283186) = 0.0
 * f(3.584428,-1.848126) = 0.0
 */
void Himmelsblau2D(int nd, double *current_model, double *misfit)
{
    double x,y,funcval,minval;

    x = current_model[0];
    y = current_model[1];

    funcval = pow((x*x +y-11),2) + pow((x+y*y-7),2);
    minval = 0.0;

    // misfit
    *misfit = sqrt( (funcval-minval)*(funcval-minval) );

    return;
}
//-----------------------------------------------------------------------------
/*
 * from: http://www.mathworks.com/matlabcentral/fileexchange/23972-chebfun/content/chebfun/examples/opt/html/Rosenbrock.html#2
 *
 */
void MultipleLocalMinima2D(int nd, double *current_model, double *misfit)
{
    double x,y,funcval,minval;

    x = current_model[0];
    y = current_model[1];

    funcval = exp(x-2*x*x-y*y)*sin(6*(x+y+x*y*y))+1;
    minval = 0.0;

    // misfit
    *misfit = sqrt( (funcval-minval)*(funcval-minval) );

    return;
}
//-----------------------------------------------------------------------------
/*
 * from: http://en.wikipedia.org/wiki/Rastrigin_function
 *
 * global minimum at (0.0), where xi E[-5.12,5.12]
 *
 */
void RastriginND(int nd, double *current_model, double *misfit)
{
    double x,funcval,minval;
    double const pi=3.141592653589793238462643383279502884197169399375105820974944592;

    minval = 0.0;
    funcval = 0.0;
    for(int k=0;k<nd;k++){
        x = current_model[k];
        funcval = funcval + (x*x-10*cos(2*pi*x));
    }
    funcval = 10*nd+funcval;

    // misfit
    *misfit = sqrt( (funcval-minval)*(funcval-minval) );

    return;
}
//-----------------------------------------------------------------------------
/*
 *
 * from: http://en.wikipedia.org/wiki/Rosenbrock_function
 * 1 minimum for N=3 f(1,1,1)
 * 4 <= N <= 7 global min at f(1,1,...,1)
 * local min af f(-1,1,...,1)
 *
 */
void RosenbrockND(int nd, double *current_model, double *misfit)
{
    double x,xp1,funcval,minval;
    int k;


    minval = 0.0;
    funcval = 0.0;

    for(k=0;k<nd-1;k++){
        x   = current_model[k];
        xp1 = current_model[k+1];
        funcval = funcval + (pow(1-x,2)+100*pow(xp1-x*x,2));
    }

    // misfit
    *misfit = sqrt( (funcval-minval)*(funcval-minval) );

    return;
}
//-----------------------------------------------------------------------------
/*
 *
 *
 */
void EffViscosity(double *current_model, double *mu_eff)
{
    double e0,R,mu0,eII,n,E,V,T,P;

    e0  = 1e-15;          // 1/s
    R   = 8.3145;
    mu0 = pow(10,current_model[0]); //  13     18
    eII = pow(10,current_model[1]); // -30    -12
    n   = current_model[2];         //   1      5
    E   = 1e3 * current_model[3];   //  50    600
    V   = 1e-6* current_model[4];   //   0     25
    T   = current_model[5];         // 300   1650
    P   = pow(10,current_model[6]); //   5     12

    *mu_eff = log10(mu0 * pow(eII/e0, 1.0/(n-1.0)) * exp((E+P*V)/(n*R*T)));

    double mu_target= 21.5; // 25.0-18.0;
    if (*mu_eff > 25 || *mu_eff < 18)
    {
    	*mu_eff = 50;
    }
    else
    {
    	*mu_eff = fabs(*mu_eff-mu_target);
    }

//    if (*mu_eff <= 21.5)
//    {
//    	*mu_eff = 21.5 - *mu_eff;
//    }




    return;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/*
 * This is a wrapper to compiled MATLAB misfit functions
 *
 */
void forward_matlab(int nd, double *current_model, double *misfit,MPI_Comm fwrd_comm,int opt_cpu,int fwrd_group_id,int cntModels,int IorM,double &diff_t)
{
#ifdef MATLAB
    time_t          start_t,end_t;
    double         *current_buffer;
    mwArray         input((mwSize)(nd+2),1,mxDOUBLE_CLASS);
    mwArray         mfit(1,1,mxDOUBLE_CLASS);

    // Create a buffer where to store the ID  and model parameters
    current_buffer    = new double[nd+2];
    current_buffer[0] = (double) fwrd_group_id;
    current_buffer[1] = (double) cntModels;
    for(int k=0;k<nd;k++)
    {
        current_buffer[k+2] = current_model[k];
    }

    // Initial time stamp
    time(&start_t);
    FwrdModInterface_Initialize(fwrd_comm,nd,current_model,cntModels,fwrd_group_id,start_t);

    // C ++ matlab interface
    input.SetData(current_buffer,nd+2);
    fwrd_mod_NAplusFullInv(1,mfit,input);
    *misfit = mfit.Get(1,1);

    // Final time stamp
    time(&end_t);
    diff_t = difftime(end_t,start_t);
    FwrdModInterface_Finalize(fwrd_comm,*misfit,cntModels,diff_t);

    // command line display
    if(opt_cpu == 0)
    {
        if (IorM==0)
            printf(" %5d   %5d   %6.0fs   init\n",cntModels,fwrd_group_id,diff_t);
        else
            printf(" %5d   %5d   %6.0fs   main\n",cntModels,fwrd_group_id,diff_t);
    }

    delete [] current_buffer;

    return;
#endif    
}

//-----------------------------------------------------------------------------
void FwrdModInterface_Initialize(MPI_Comm fwrd_comm, int nd, double *current_model,int cntModels,int fwrd_group_id,time_t start_t)
{
    char              *filename_groupX,*parameters,*temp;
    struct tm         *timeinfo;
    size_t             N=20;
    char               time_now[N];
    static const char *format = "[%d]: %G"; // 10 characters
    string             str;




    // convert time
    timeinfo = localtime(&start_t);
    strftime(time_now,N,"%d-%b-%y %H:%M:%S",timeinfo);

    // redirect stdout to group specific file
    asprintf(&filename_groupX,"LOG_NAplus_%s_Group%d.txt",naplus.fileID,fwrd_group_id);
    freopen (filename_groupX,"a", stdout);
    free(filename_groupX);

    // create string
    for (int k=0;k<nd;k++){
        asprintf(&temp,format,k,current_model[k]);
        str += temp;
        str += " ";
        free(temp);
    }
    asprintf(&parameters,str.c_str());

    // print info
    printf("\n\n");
    printf("*******************************************************************************************************************\n");
    printf("   > time      : %s\n",time_now);
    printf("   > start model no. : %d\n",cntModels);
    printf("   > group id. : %d\n",fwrd_group_id);
    printf("   > parameters: %s\n",parameters);

    // free options
    free(parameters);

    return;
}

//-----------------------------------------------------------------------------
void FwrdModInterface_Finalize(MPI_Comm fwrd_comm,double misfit, int cntModels,double diff_t)
{
    string     filename_stdout,strfileID;

    // print info
    printf("\n");
    printf("   > finished model no. : %d\n",cntModels);
    printf("   > misfit    : %G\n",misfit);
    printf("   > duration  : %G sec\n",diff_t);
    printf("*******************************************************************************************************************\n");

#ifdef REDIRECT_STDOUT
    // redirect stdout to stdout-file
    strfileID=naplus.fileID;
    filename_stdout = "STDOUT_NAplus_" + strfileID + ".txt";
    freopen (filename_stdout.c_str(),"a", stdout);
#else
    // redirect stdout to screen
    freopen("/dev/tty", "w",stdout);
#endif

    return;
}

