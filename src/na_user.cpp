/* 
 * na_user.cpp
 * 
*/

#include "na_user.h"



//=============================================================================
void PRNGCreate(NaUserContext &user)
{
	user.prngstream = new RngStream[user.opt_size];
	return;
}
//-----------------------------------------------------------------------------
void PRNGDestroy(NaUserContext &user)
{
	delete [] user.prngstream;
	return;
}
//-----------------------------------------------------------------------------

// Advance the state of all streams by a full substream in case of restart
void PRNGAdvanceState(NaUserContext &user)
{
	if(user.manadvance)
	{
		user.restart = user.restart + user.manadvance;
		if(user.opt_rank) printf("[NAPlus:  all] Found manual advance option \n");
	}

	if(user.opt_rank) printf("[NAPlus:  all] Advance state of random number sub-streams %d times \n",user.restart);

	for(int k=0;k<user.restart;k++)
		user.prngstream[user.opt_rank].ResetNextSubstream();
	return;
}
//-----------------------------------------------------------------------------
void PRNGReset(NaUserContext &user)
{	
	user.prngstream[user.opt_rank].ResetStartSubstream();
	return;
}
//-----------------------------------------------------------------------------
void PRNGUnfRand01(RngStream *stream, int rank,double *rnd)
{
	//random double between 0 and 1 (including 0 and 1)
	*rnd = stream[rank].RandU01();
	return;
}
//-----------------------------------------------------------------------------
void PRNGUnfRandAB(RngStream *stream, int rank, double a, double b,double *rnd)
{
	//random double between a and b (including a and b)
	*rnd = a + (b - a) * stream[rank].RandU01();
	return;
}
//-----------------------------------------------------------------------------
void PRNGUnfIntRandAB(RngStream *stream, int rank , int a, int b, int *rnd)
{
	// random integer between a and b (including a and b)
	*rnd = (int) stream[rank].RandInt(a,b);
	return;
}
//-----------------------------------------------------------------------------
void DiscParamRange(NaUserContext &user,double *discrange)
{

    double  minrange=0.0,maxrange = 1.0, ddisc;

    // increment
    ddisc = (maxrange-minrange) / (double) user.numdisc;

    discrange[0] = ddisc/2.0;
    for (int k=1;k<user.numdisc;k++)
    {
        discrange[k] = discrange[k-1] + ddisc;
    }
    return;
}
//-----------------------------------------------------------------------------

void prng_unfrandab(RngStream *stream   , int rank, double a, double b, double *rnd)
{
	PRNGUnfRandAB(stream,rank,a,b,rnd);
	return;
}
void prng_unfrand01(RngStream *stream, int rank,double *rnd)
{
	PRNGUnfRand01(stream,rank,rnd);
	return;
}
void prng_unfintrandab(RngStream *stream, int rank, int    a, int    b, int    *rnd)
{
	PRNGUnfIntRandAB(stream,rank,a,b,rnd);
	return;
}
//=============================================================================
//-----------------------------------------------------------------------------
/*
void GetQuasiRandomSequence(NaUserContext &user,double *all_models_init,double *ranget)
{

#ifdef GSL
//http://www.gnu.org/software/gsl/manual/html_node/Quasi_002drandom-number-generator-algorithms.html#Quasi_002drandom-number-generator-algorithms

    double *current_model;
    gsl_qrng * generator = gsl_qrng_alloc(gsl_qrng_sobol,(unsigned int) user.nd);
    
    // allocate space
    current_model = new double[user.nd];

    // Set samples to table of initial models
    for (int k=0;k<user.InitSize;k++)
    {
        gsl_qrng_get(generator,current_model);
        for (int kk=0;kk<user.nd;kk++)
            all_models_init[user.nd*k+kk] = (1.0-current_model[kk])*ranget(0,kk) + current_model[kk]*ranget(1,kk);
    }

    // free allocated array & objects
    delete [] current_model;
    gsl_qrng_free(generator);

    return;
#endif
}



//-----------------------------------------------------------------------------
void GetDiscretePseudoRandomSequence(NaUserContext &user,double *all_models_init,double *ranget)
{
#ifdef GSL
    
    
    const gsl_rng_type *T;
    gsl_rng *r;
    
    double *discrange, a,b;
    double  minrange=0.0,maxrange = 1.0, ddisc;
    
    
    // Allocate discrete vector
    discrange = new double[user.numdisc];
    
    // increment
    ddisc = (maxrange-minrange) / (double) user.numdisc;
    
    
    discrange[0] = ddisc/2.0; 
    for (int k=1;k<user.numdisc;k++)
    {
        discrange[k] = discrange[k-1] + ddisc;
    }
    
    
    
    // Initialize random number generato
    gsl_rng_env_setup();
    
    T = gsl_rng_default;
    r = gsl_rng_alloc(T);
        
    gsl_rng_set(r,(unsigned long int) user.iseed);
    //  gsl_rng_set(r,(unsigned long int) 5489);
    //  printf("generator name %s \n",gsl_rng_name(r));
    //  printf("generator default seed %lu \n",gsl_rng_default_seed);
    


    for(int k=0;k<user.InitSize*user.numrandcycle;k++)
    {
        for(int kk=0;kk<user.nd;kk++)
        {
            unsigned long int u =  gsl_rng_uniform_int(r,user.numdisc);
        }
    }

    
    for(int k=0;k<user.InitSize;k++)
    {
        for(int kk=0;kk<user.nd;kk++)
        {
            unsigned long int u =  gsl_rng_uniform_int(r,user.numdisc);
            //a = FSUB(ran3)(&user.iseed);
            
            a = discrange[(int) u];
            b = 1.0 - a;
            all_models_init[user.nd*k+kk] = b*ranget(0,kk) + a*ranget(1,kk);
        }
    }
    


    // free allocated memory
    delete [] discrange;
    gsl_rng_free(r);
    
    

    return;
#endif
}
*/
//-----------------------------------------------------------------------------
/*
void GetPseudoRandomSequence(NaUserContext &user,double *all_models_init,double *ranget)
{
    double a,b;

    for(int k=0;k<user.InitSize;k++)
    {
        for(int kk=0;kk<user.nd;kk++)
        {
            a = FSUB(ran3)(&user.iseed);
            b = 1.0 - a;
            all_models_init[user.nd*k+kk] = b*ranget(0,kk) + a*ranget(1,kk);
        }
    }
    return;
}
*/

//-----------------------------------------------------------------------------
void na_shuffle(NaUserContext &user, int *iarr,double *arr,int n)
{
	// randomly re-arranges input array
	int k,ival;
	double val;

	for (int j=0; j<n;j++)
	{
		   PRNGUnfIntRandAB(user.prngstream, user.opt_rank,0,n,&k);

		   // exchange indices
		   ival = iarr[j];
		   iarr[j] = iarr[k];
		   iarr[k] = ival;

		   // exchange values
		   val = arr[j];
		   arr[j] = arr[k];
		   arr[k] = val;
	}

	return;
}

//-----------------------------------------------------------------------------
void na_initialize(NaUserContext &user, double *ranget, double *all_misfits, double *all_models, double *xcur)
{

	//     Initialize some arrays
	for (int k=0;k<(user.nd * user.nummodels);k++)
		all_models[k] = 0.0;

	for (int k=0;k<(user.nummodels);k++)
		all_misfits[k] = 0.0;

	//     set logical switch for first call to NA_sample (ensures distance list is initialized)
	user.restartna = 1;


	//     Normalize parameter ranges by a-priori model co-variances
	if(user.scales_in[0] == 0)
	{
	//        First option: No transform (All a priori model co-variances are equal to unity)
		for (int k=0; k<(user.nd); k++)
		{
			ranget(0,k) = user.ranges_in(0,k);
			ranget(1,k) = user.ranges_in(1,k);
			user.scales_in[k+1] = 1.0;
		}
	}
	else if(user.scales_in[0] == -1.0)
	{
	//        Second option: Use parameter ranges as a priori model co-variances
		for (int k=0; k<(user.nd); k++)
		{
			ranget(0,k) = 0.0;
			ranget(1,k) = 1.0;
			user.scales_in[k+1] = user.ranges_in(1,k) - user.ranges_in(0,k);
		}
	}
	else
	{
	//        Third option: Use scales array as a priori model co-variances
		for (int k=0; k<(user.nd); k++)
		{
			if(user.scales_in[k+1] == 0.0)
			{
				// ERROR
			}

			ranget(0,k)  = 0.0;
			ranget(1,k)  = (user.ranges_in(1,k)-user.ranges_in(0,k)) / user.scales_in[k+1];

		}
	}

	//     calculate axis increments and initialize current point (used by NA_sample) to mid-point of parameter space
	for (int k=0; k<(user.nd); k++)
		xcur[k] = (ranget(1,k) + ranget(0,k)) * 0.5;


    // Initialize random number generator
    PRNGCreate(user);

	return;
}

//-----------------------------------------------------------------------------
void na_misfit(NaUserContext &user,NaMfitStatistics &stats,double *all_misfits,int ntot,int nsample,int it,int ncells)
{
    int iopt,k,ntotal;
    int     iselect;

    stats.mfit_minc = all_misfits[ntot];
    stats.mfit_mean = stats.mfit_minc;
    iopt = ntot+1;
    ntotal = ntot+nsample;

    // find lowest misfit
    for(k=ntot+2;k<=ntotal;k++){
        stats.mfit_mean = stats.mfit_mean + all_misfits[k-1];
        if(all_misfits[k-1] <stats.mfit_minc){
            stats.mfit_minc = all_misfits[k-1];
            iopt=k;
        }
    }
    stats.mfit_mean = stats.mfit_mean/((double) nsample);


    if((stats.mfit_minc<stats.mfit_min) || it==1){
        stats.mfit_opt = iopt;
        stats.mfit_min = stats.mfit_minc;
    }

    // find models with lowest ncells misfit values
    if(ncells==1){
        stats.ff_mfit_ord[0] = stats.mfit_opt;
    }
    else{
        
        // write all misfit values to ==> work
        // create array with indices with size of work ==> ff_ind 
        for(k=0;k<ntotal;k++){
            stats.ff_ind[k]  = k+1; // array contains  of fortran array(1,...,N)
            stats.work[k] = all_misfits[k];
        }

        // jumble initial indices to randomize order of models when misfits are equal
        //FSUB(na_jumble)(stats.ff_ind,stats.work,&ntotal,&user.iseed);
        na_shuffle(user, stats.ff_ind,stats.work,ntotal);
        
        // order ff_ind according to the content of work ==> ff_ind
        FSUB(na_select)(&ncells,&ntotal,stats.work,stats.ff_ind,&iselect);


        // copy the ncells best model indices to ff_iwork ==> ff_iwork 
        for(k=0;k<ncells;k++)
            stats.ff_iwork[k] = stats.ff_ind[k];

        //order misfit of lowest ncells
         FSUB(na_indexx)(&ncells,stats.work,stats.ff_ind);

        // Store the indices of ncells lowest misfit values in all_misfits ==> ff_mfit_ord
        for(k=0;k<ncells;k++)  // access with fortran indices (1,..,N) - 1
            stats.ff_mfit_ord[k] = stats.ff_iwork[stats.ff_ind[k]-1];
    }
    return;
}
//-----------------------------------------------------------------------------
void stats_initialize(NaUserContext &user,NaMfitStatistics &stats){

    // declare variables
    stats.mfit_min    = 0.0;
    stats.mfit_minc   = 0.0;
    stats.mfit_mean   = 0.0;
    stats.mfit_opt    = 0;

    // declare arrays
    stats.ff_mfit_ord = new int[user.nummodels];
    stats.ff_ind      = new int[user.nummodels];
    stats.ff_iwork    = new int[user.nummodels];
    stats.work        = new double[user.nummodels];

    // initialize arrays
    for(int k=0;k<user.nummodels;k++){
        stats.ff_mfit_ord[k] = 0;
        stats.ff_ind[k]      = 0;
        stats.ff_iwork[k]    = 0;
        stats.work[k]        = 0.0;
    }
    return;
}
//-----------------------------------------------------------------------------
void stats_finalize(NaMfitStatistics &stats){

    delete [] stats.ff_mfit_ord;
    delete [] stats.ff_ind;
    delete [] stats.ff_iwork;
    delete [] stats.work;

    return;
}

//-----------------------------------------------------------------------------
void wbuff_initialize(NaUserContext &user,NaWriteBuffer &wbuff)
{
    wbuff.size    = user.nd+5;
    wbuff.ind     = 0;
    wbuff.ibuff   = new double[wbuff.size*user.numwbuff];
    return;
}

//-----------------------------------------------------------------------------
void wbuff_finalize(NaWriteBuffer &wbuff)
{
    delete [] wbuff.ibuff;
    return;
}
//-----------------------------------------------------------------------------
void check_err(MPI_Comm comm, int err_code)
{
//     MPI_SUCCESS - kein Fehler
//     MPI_ERR_BUFFER - ungueltiger Pufferzeiger
//     MPI_ERR_COUNT - ungueltiges count-Argument
//     MPI_ERR_TYPE - ungueltige Datentypangabe
//     MPI_ERR_TAG - ungueltiges tag-Argument
//     MPI_ERR_COMM - ungueltiger Kommunikator
//     MPI_ERR_RANK - ungueltiger Rang
//     MPI_ERR_REQUEST - ungueltiges Request-handle
//     MPI_ERR_ROOT - ungueltiges root-Argument
//     MPI_ERR_GROUP - ungueltige Gruppe
//     MPI_ERR_OP - ungueltige Operation
//     MPI_ERR_TOPOLOGY - ungueltige Topologie
//     MPI_ERR_DIMS - ungueltige Dimensionsangabe
//     MPI_ERR_ARG - irgendein ungueltiges anderes Argument
//     MPI_ERR_UNKNOWN - unbekannter Fehler
//     MPI_ERR_TRUNCATE - Nachricht wurde beim Empfaenger abgeschnitten
//     MPI_ERR_OTHER - anderer (bekannter) Fehler, der nicht in dieser Liste steht
//     MPI_ERR_INTERN - interner MPI-Fehler
//     MPI_ERR_LASTCODE - hoechster Standard-Fehlercode

    if (err_code != MPI_SUCCESS){

        int  rank, err_length, err_class;
        char err_msg[MPI_MAX_ERROR_STRING];

        MPI_Comm_rank(comm,&rank);

        // get interpretation of err_code
        MPI_Error_class(err_code, &err_class);

        // print err_code
        MPI_Error_string(err_code, err_msg, &err_length);
        fprintf(stderr, "%3d: %s\n", rank, err_msg);

        // print err_class
        MPI_Error_string(err_class, err_msg, &err_length);
        fprintf(stderr, "%3d: %s\n", rank, err_msg);

        MPI_Abort(comm,err_code);
    }
}
//-----------------------------------------------------------------------------
double * make_matrix(int m, int n)
{      //make_matrix(num_row,num_col)
    double *a = new double[m*n];
    double num = 0.0;

    for(int j(0); j < n; j++)     //cols
    {   for(int i(0); i < m; i++) //rows
        {
            a(i, j) = num;
            num=num+1;
        }
    }
    return a;
}
//-----------------------------------------------------------------------------
void UserContext_initialize(NaUserContext &user){

    // Initialize
    user.nd           =  0;
    user.nsamplei     =  0;
    user.nsamplei_file=  0;
    user.nsample      =  0;
    user.restart      =  0;
    user.manadvance   =  0;
    user.ncells       =  0;
    user.itmax        =  0;
    user.nummodels    =  0;
    user.restartna    =  0;
    user.nainit_idnext=  0;
    user.initsamp     =  0;
    user.numwbuff     =  0;
    user.numdisc      = 50;

    if(user.opt_rank==0){
        printf("--------------------------------------------------------------------------------\n");
        printf("User struct:\n");
        printf("============\n");
    }
    
    // Inputfile
    LoadParamFile(user);

    // Corrections
    user.nummodels = (user.nsamplei)+(user.itmax)*(user.nsample);

    if(user.opt_rank==0){
        printf("nsamplei:%d\n",user.nsamplei);
        printf("nsample:%d\n",user.nsample);
        printf("restart:%d\n",user.restart);
        printf("ncells:%d\n",user.ncells);
        printf("itmax:%d\n",user.itmax);
        printf("nd:%d\n",user.nd);
        printf("nummodels:%d\n",user.nummodels);
        printf("initsamp:%d\n",user.initsamp);
        printf("numwbuff:%d\n",user.numwbuff);
        printf("--------------------------------------------------------------------------------\n");
    }
    if(user.nd < 0) printf("[NAplus Error] Wrong input parameter \n");

    if((user.ncells>user.nsample) || (user.ncells>user.nsamplei)) printf("[NAplus Error] ncells should not be greater than nsample or nsamplei \n");

    return;
}
//-----------------------------------------------------------------------------
void UserContext_finalize(NaUserContext &user){

    delete [] user.ranges_in;
    delete [] user.scales_in;
    return;
}
//-----------------------------------------------------------------------------
void LoadParamFile(NaUserContext &user){
    
    FILE *ifp;
    int found;
    
    double testvars[2],testvar;
    int nvalues;
    int k;
    char rangechar[20];
    
    // open na.in
    ifp = fopen("na.in", "r");
    if (ifp == NULL) {
        fprintf(stderr, "[NAplus Error] Can't open input file na.in \n");
        exit(1);
    }

    // define variables to open
    parse_GetInt(ifp,"NumberOfInitialSamples", &user.nsamplei, &found );
    parse_GetInt(ifp,"NumOfSamples", &user.nsample, &found );
    parse_GetInt(ifp,"NumOfCells2Resample", &user.ncells, &found );
    parse_GetInt(ifp,"NumOfIterations", &user.itmax, &found );
    parse_GetInt(ifp,"Restart", &user.restart, &found );
    parse_GetInt(ifp,"ManAdvance", &user.manadvance, &found );
    parse_GetInt(ifp,"ParamDim", &user.nd, &found );
    parse_GetInt(ifp,"InitSamp", &user.initsamp, &found );
    parse_GetInt(ifp,"WriteBufferSize", &user.numwbuff, &found );
    parse_GetInt(ifp,"NumDiscreteVal", &user.numdisc, &found );
    
    // allocate array for ranges
    user.ranges_in  =  make_matrix(2,user.nd);

    for(k=0;k<(user.nd);k++){
        sprintf(rangechar,"ParamRange[%d]",k);
        parse_GetDoubleArray(ifp,rangechar, &nvalues, testvars, &found );
        if(nvalues > 2) printf("[NAplus Warning] ParamRange should not have more than 2 entries\n"); 
        if(user.opt_rank==0)printf("ParamRange[%d] = [%f,%f] \n",k,testvars[0],testvars[1]);
        user.ranges_in(0,k) = (double) (testvars[0]);
        user.ranges_in(1,k) = (double) (testvars[1]);
    }

    // allocate array for scales
    user.scales_in = new double[user.nd+1];
    
    parse_GetDouble(ifp,"ParamAutoScale",&testvar, &found );
    user.scales_in[0] = (double) (testvar);
    if(user.opt_rank==0)printf("ParamAutoScale= %g \n",testvar);

	for(k=0;k<(user.nd);k++)
	{
		sprintf(rangechar,"ParamScale[%d]",k);
		if (user.scales_in[0] != -1.0)
		{
			parse_GetDouble(ifp,rangechar, &testvar, &found );
		}
		else
		{
			testvar = 1.0;
		}
		if(user.opt_rank==0)printf("ParamScale[%d] = %g \n",k,testvar);
		user.scales_in[k+1] = (double) (testvar);
	}


    //~ for(k=0;k<(user.nd);k++){
        //~ sprintf(rangechar,"ParamName[%d]",k);
        //~ parse_GetString(ifp,rangechar, &user.doublevar,MAX_STRING_LENGTH, &found );
    //~ }

    fclose(ifp);

    return;
}
//-----------------------------------------------------------------------------
void write_models(NaUserContext &user,NaCurrentResult &cr,double mCount,double mID,int sourceID, const char *prefix){
    FILE *fileID;
    char *filename;
    double sID;//,mCount;
    //mCount = (double) cr.cntModels;
    sID    = (double) sourceID;
    const int num=1;

    if(asprintf(&filename,"%sID%d.bin",prefix,user.opt_rank)<0) printf("[NAplus Error: %d] asprintf in funct write_models",user.opt_rank);
    fileID = fopen(filename,"a");
    free(filename);
        if(IS_LITTLE_ENDIAN){ // swap to big endian
            ByteSwap(&mCount,1);
            ByteSwap(&mID,1);
            ByteSwap(&sID,1);
            ByteSwap(cr.current_model,user.nd);
            ByteSwap(&cr.current_misfit,1);
            ByteSwap(&cr.diff_t,1);
        }

        fwrite(&mCount,sizeof(double),1,fileID);
        fwrite(&mID,sizeof(double),1,fileID);
        fwrite(&sID,sizeof(double),1,fileID);
        fwrite(cr.current_model,sizeof(double),user.nd,fileID);
        fwrite(&cr.current_misfit,sizeof(double),1,fileID);
        fwrite(&cr.diff_t,sizeof(double),1,fileID);

        if(IS_LITTLE_ENDIAN){ // swap back - it might be used again
            ByteSwap(cr.current_model,user.nd);
            ByteSwap(&cr.current_misfit,1);
            ByteSwap(&cr.diff_t,1);
        }
    fclose(fileID);
    return;
}
//-----------------------------------------------------------------------------
void buffwrite_models(NaUserContext &user,NaWriteBuffer &wbuff,NaCurrentResult &cr,double mCount,double mID,int sourceID, const char *prefix)
{
    FILE *fileID;
    char *filename;
    const int num=1;

    // Write models to a buffer. It's size depends on user.numwbuff
    wbuff.ibuff[wbuff.ind*wbuff.size+0] = mCount;
    wbuff.ibuff[wbuff.ind*wbuff.size+1] = mID;
    wbuff.ibuff[wbuff.ind*wbuff.size+2] = (double) sourceID;
    for(int i=0;i<user.nd;i++)
        wbuff.ibuff[wbuff.ind*wbuff.size+3+i] = cr.current_model[i];
    wbuff.ibuff[wbuff.ind*wbuff.size+user.nd+3] = cr.current_misfit;
    wbuff.ibuff[wbuff.ind*wbuff.size+user.nd+4] = cr.diff_t;
    
    wbuff.ind++;


    // write to file if condition is met
    if (wbuff.ind == user.numwbuff)
    {
        if(asprintf(&filename,"%sID%d.bin",prefix,user.opt_rank)<0) printf("[NAplus Error: %d] asprintf in funct write_models",user.opt_rank);
        fileID = fopen(filename,"a");
        free(filename);
        
             // swap to big endian
            if(IS_LITTLE_ENDIAN)
            {
                ByteSwap(wbuff.ibuff,user.numwbuff*wbuff.size);
            }
            
            fwrite(wbuff.ibuff,sizeof(double),user.numwbuff*wbuff.size,fileID);
            
            // swap back - it might be used again
            if(IS_LITTLE_ENDIAN)
            {
                ByteSwap(wbuff.ibuff,wbuff.ind*wbuff.size);
            }
        fclose(fileID);
        
        wbuff.ind=0;
    }
    return;
}
//-----------------------------------------------------------------------------
void write_header(NaUserContext &user,const char *prefix){

    FILE *fileID;
    char *filename;
    const int num=1;
    int restart,opt_rank,opt_size,nd;

    if(asprintf(&filename,"%sID%d.bin",prefix,user.opt_rank)<0) printf("[NAplus Error: %d] asprintf in funct write_header",user.opt_rank);
    fileID = fopen(filename,"w");
    free(filename);

    	restart=user.restart;
        opt_rank=user.opt_rank;
        opt_size=user.opt_size;
        nd=user.nd;
        if(IS_LITTLE_ENDIAN){ // swap to big endian
        	ByteSwap(&restart,1);
        	ByteSwap(&opt_rank,1);
            ByteSwap(&opt_size,1);
            ByteSwap(&nd,1);
            ByteSwap(user.ranges_in,2*user.nd);
        }

        fwrite(&restart,sizeof(int),1,fileID);
        fwrite(&opt_rank,sizeof(int),1,fileID);
        fwrite(&opt_size,sizeof(int),1,fileID);
        fwrite(&nd,sizeof(int),1,fileID);
        fwrite(user.ranges_in,sizeof(double),2*user.nd,fileID);

        if(IS_LITTLE_ENDIAN){ // swap back
            ByteSwap(user.ranges_in,2*user.nd);
        }
    fclose(fileID);
    return;
}
//-----------------------------------------------------------------------------
void read_models(NaUserContext &user,NaCurrentResult &cr,double *all_models,double *all_misfits,const char *prefix){

    FILE   *fileID;
    int     filesize;
    int     position,headersize,dummy_int,file_nd;
    double *buffer,*buffer_ranges;
    int     restart,Nheader=4,Nbuffer=user.nd+5;
    char   *filename;
    double *raw_model,*all_models_raw;

    // Allocate arrays for raw models
    all_models_raw = new double[user.nd * user.nummodels];
    raw_model      = new double[user.nd];

    if(!user.opt_rank) printf("[NAPlus:  all] Read file: %s on all processes.\n",prefix);

    // open file 
    if(asprintf(&filename,"%s",prefix)<0) printf("[NAplus Error: %d] asprintf in funct read_models \n",user.opt_rank);
    fileID = fopen(filename,"r");
    if (fileID == NULL)
    {
        fprintf(stderr, "[NAplus Error: %d] Can't open file: %s \n",user.opt_rank,filename);
        exit(1);
    }
    free(filename);

    // obtain file size
    fseek (fileID , 0 , SEEK_END);
    filesize = (int)ftell(fileID);
    rewind (fileID);
    //printf("[NAplus] filesize %d \n",filesize);

    // read header information
    position = 0;
    read_array(&restart,1,&position,fileID);               // Header: restart
    read_array(&dummy_int,1,&position,fileID);             // Header: rank (not needed)
    read_array(&dummy_int,1,&position,fileID);             // Header: size (not needed)
    read_array(&file_nd,1,&position,fileID);               // Header: number of model parameter
    if(file_nd != user.nd)
    {
        fprintf(stderr, "[NAplus Error: %d] Model dimension in file (%d) != User model dimension (%d) \n",user.opt_rank,file_nd,user.nd);
        exit(1);
    }
    buffer_ranges = new double[2*user.nd];
    read_array(buffer_ranges,2*user.nd,&position,fileID);  // Header: model ranges
    for(int k=0;k<(2*user.nd);k++)
    {
        if(buffer_ranges[k] != user.ranges_in[k])
        {
            fprintf(stderr, "[NAplus Error: %d] ranges of file are different form user.ranges_in \n",user.opt_rank);
            exit(1);
        }
    }
    delete[] buffer_ranges;
    headersize = Nheader*(int)sizeof(int) + 2*user.nd * (int)sizeof(double) ;

    // determine the number of models (all are of double precision)
    user.nsamplei_file = (filesize-headersize)/(int)sizeof(double)/(Nbuffer);
    if(user.nsamplei_file >= user.nummodels)
    {
        fprintf(stderr, "[NAplus Error: %d] Number of models in file (%d) >= Allocated number of models (%d) \n",user.opt_rank,user.nsamplei_file,user.nummodels);
        exit(1);
    }

    // allocate buffer to read a full model + misfit + modelID
    buffer = new double[Nbuffer];

    // read data
    while(position<filesize)
    {
        read_array(buffer,Nbuffer,&position,fileID);
        //modelcnt = (int) buffer[0];                      // Data: modelID - DON'T READ
        //mID    = (int) buffer[1];                        // Data: mID     - DON'T READ
        //sID    = (int) buffer[2];                        // Data: sID     - DON'T READ
        for(int k=0;k<user.nd;k++)                             // Data: model
            all_models_raw[(cr.cntModels*user.nd)+k] = buffer[k+3];
        all_misfits[cr.cntModels] = buffer[user.nd+3];     // Data: misfit
        //diff_t = buffer[user.nd+4];                      // Data: time     - DON'T READ
        cr.cntModels++;
    }

    // deallocate buffer
    delete[] buffer;

    // close file 
    fclose(fileID);

    if(!user.opt_rank) printf("[NAPlus:  all] Found %d models \n",user.nsamplei_file);
    
    // Scale raw models and put them to all_models
    for(int k=0;k<user.nsamplei_file;k++)
    {
        for(int kk=0;kk<user.nd;kk++)
            raw_model[kk] = all_models_raw[user.nd*k+kk];
        FSUB(transform2sca)(raw_model,&user.nd,user.ranges_in,user.scales_in,cr.sca_model);
        for(int kk=0;kk<user.nd;kk++)
            all_models[user.nd*k+kk] = cr.sca_model[kk];
    }

    // Advance each substream as many times as indicated be user.restart (other (sub) streams are not influenced)
    user.restart = restart + 1;

    if(!user.opt_rank) printf("[NAPlus:  all] This is restart no. %d \n",user.restart);

    // Deallocate temporary arrays
    delete [] all_models_raw;
    delete [] raw_model;

    return;
}
//-----------------------------------------------------------------------------
 float** Create2dArray_float(int arraySizeX, int arraySizeY) {  
    int     i;
    float   **theArray;  
    
    theArray = (float**) malloc(arraySizeX*sizeof(float*));  
    for (i = 0; i < arraySizeX; i++)  
       theArray[i] = (float*) malloc(arraySizeY*sizeof(float));  
       return theArray;  
 }
//-----------------------------------------------------------------------------
void Destroy2dArray_float(float **myArray, int nx){
    
    int i;
    
    for (i = 0; i < nx; i++)
        free(myArray[i]);
    free(myArray);
    return;
}
//-----------------------------------------------------------------------------
 float** Create2dArray_float_cpp(int m, int n) {
/*
 * declares:
 * float a[m][n];
 * 
 */  
 int i;
  
  float **a = new float* [m];
  a[0] = new float [m*n];
  for(i = 1; i < m; i++)
    a[i] = a[i-1] + n;
    
    return a;
}

//=============================================================================
// subroutines to read input file
//=============================================================================




//-----------------------------------------------------------------------------
/*
Courtesy of
http://gd.tuwien.ac.at/languages/c/cref-mleslie/CONTRIB/SNIP/trim.c
*/
#define NUL '\0'
char *trim(char *str)
{
    char *ibuf = str, *obuf = str;
    int i = 0, cnt = 0;

    /*
    **  Trap NULL
    */

    if (str)
    {
        /*
        **  Remove leading spaces (from RMLEAD.C)
        */

        for (ibuf = str; *ibuf && isspace(*ibuf); ++ibuf)
                ;
        if (str != ibuf)
                memmove(str, ibuf, ibuf - str);

        /*
        **  Collapse embedded spaces (from LV1WS.C)
        */

        while (*ibuf)
        {
            if (isspace(*ibuf) && cnt)
                    ibuf++;
            else
            {
                if (!isspace(*ibuf))
                        cnt = 0;
                else
                {
                    *ibuf = ' ';
                    cnt = 1;
                }
                obuf[i++] = *ibuf++;
            }
        }
        obuf[i] = NUL;

        /*
        **  Remove trailing spaces (from RMTRAIL.C)
        */

        while (--i >= 0)
        {
            if (!isspace(obuf[i]))
                    break;
        }
        obuf[++i] = NUL;
    }
    return str;
}
//-----------------------------------------------------------------------------
int is_comment_line( const char line[] )
{
    int i,L;

    L = strlen(comments);
    for( i=0; i<L; i++ ) {
        if( line[0] == comments[i] ) {
            return _TRUE;
        }
    }

    return _FALSE;
}
//-----------------------------------------------------------------------------
/* removes all characters before and including the = sign */
void strip( char line[] )
{
    int i,line_L;

    line_L = strlen(line);

    for( i=0; i<line_L; i++ ) {
        if( line[i] != '=' ) {
            line[i] = ' ';
        }
        else {
            line[i] = ' ';
            return;
        }
    }
}
//-----------------------------------------------------------------------------
void strip_L( char str[] )
{
    int count, i = 0;
    char *p = str;
    int L;

    while(*p) {
        if( (*p == ' ') || (*p == '\t') ) {
            i++;
            p++;
        }
        else { break; }
    }
    count = i;

    L = strlen( str );
    memmove( &str[0], &str[count], sizeof(char)*(L-count) );
    str[L-count] = 0;
}
//-----------------------------------------------------------------------------
/* removes all characters after and including the first comment character encountered */
void trim_past_comment( char line[] )
{
    int i,j,k,L,line_L;

    line_L = strlen(line);
    L = strlen(comments);

    for( i=0; i<line_L; i++ ) {
        for( j=0; j<L; j++ ) {
            if( line[i] == comments[j] ) {
                /* prune everything past */
                for( k=i; k<line_L; k++ ) {
                    line[k] = ' ';
                }
                /* remove white space */
                trim(line);
                return;
            }
        }
    }

}
//-----------------------------------------------------------------------------
void strip_all_whitespace( char str[], char str2[] )
{
    int i = 0;
    char *p = str;
    while(*p)
    {
        if( (*p != ' ') && (*p != '\t') )
                str2[i++] = *p;
        p++;
    }
    str2[i] = 0;
}
//-----------------------------------------------------------------------------
int key_matches( const char key[], char line[] )
{
    int key_L, line_L, i;
    int c, cmp;
    char LINE[10000];


    strip_all_whitespace(line, LINE);

    c = 0;
    line_L = strlen( LINE );
    key_L = strlen( key );

    for( i=0; i<line_L; i++ ) {
        if( LINE[i] == '=' ) {
            c = i;
            break;
        }
    }
    /* blank line or a comment line */
    if( c== 0 ) {
        return _FALSE;
    }

    /* The linelength should not be smaller than the key */
    if (c<key_L){
        return _FALSE;
    }

    cmp = strncmp(key,LINE,(c) );

    //cmp = strncmp(key,LINE,key_L );

    //printf("%s : %s --  %lld \n", key, LINE, cmp );

    if( cmp == 0 ) {
    //  printf("NAME = %s   KEY=%s, keylength=%lld, c=%lld \n", LINE, key, key_L,c );
        return _TRUE;
    }
    else {
        return _FALSE;
    }
}
//-----------------------------------------------------------------------------
/* search file look for key in the file */
void parse_GetInt( FILE *fp, const char key[], int *value, int *found )
{
    char line[MAX_LINE_LEN];
    int comment;
    int match, int_val;

    /* init */
    *found = _FALSE;

    /* reset to start of file */
    //fseek( fp, 0, SEEK_SET);
    rewind( fp );

    while( !feof(fp) ) {
        if(fgets( line, MAX_LINE_LEN-1, fp )==NULL) printf("[NAplus Error] fgets in funct parse_GetInt \n");

        /* get rid of white space */
        trim(line);

        /* is first character a comment ? */
        comment = is_comment_line( line );
        if( comment == _TRUE ) {   continue;  }

        match = key_matches( key, line );
        if( match == _FALSE ) {   continue;   }


//      printf("Searching for %s \n", key );
//      printf("  Found %s \n", line );

        /* strip word and equal sign */
        strip(line);
//      printf("  stripped %s \n", line );


        int_val = strtol( line, NULL, 0 );
//      printf("    %lld \n", int_val );
#ifdef _LOG_PARSER
        printf("Key(%s) -- Value(%lld) \n", key, (LLD)int_val );
#endif
        *value = int_val;
        *found = _TRUE;
        return;
    }
}
//-----------------------------------------------------------------------------
void parse_GetIntAllInstances( FILE *fp, const char key[], int *nvalues, int values[], int max_L, int *found )
{
    char line[MAX_LINE_LEN];
    int comment;
    int match;
    int int_val;
    int count;

    /* init */
    *found = _FALSE;
    count = 0;

    /* reset to start of file */
    rewind( fp );

    while( !feof(fp) ) {
        if(fgets( line, MAX_LINE_LEN-1, fp )==NULL) printf("[NAplus Error] fgets in funct parse_GetIntAllInstances \n");

        /* get rid of white space */
        trim(line);

        /* is first character a comment ? */
        comment = is_comment_line( line );
        if( comment == _TRUE ) {   continue;  }

        match = key_matches( key, line );
        if( match == _FALSE ) {   continue;   }


        /* strip word and equal sign */
        strip(line);

        int_val = strtol( line, NULL, 0 );

        if( max_L == count ) {
            printf("parse_GetIntAllInstances: Error, input array is not large enough to hold result \n");
            return;
        }
        values[count] = int_val;


        count++;

        *found = _TRUE;
    }

#ifdef _LOG_PARSER
    if( *found == _TRUE ) {
        int i;

        printf("Key(%s) -- Values( %lld", key, (LLD)values[0] );
        for( i=1; i<count; i++ ) {
            printf(", %lld", (LLD)values[i] );
        }printf(" )\n");
    }
#endif

    *nvalues = count;

}
//-----------------------------------------------------------------------------
void parse_GetDouble( FILE *fp, const char key[], double *value, int *found )
{
    char line[MAX_LINE_LEN];
    int comment;
    int match;
    double double_val;

    /* init */
    *found = _FALSE;

    /* reset to start of file */
    rewind( fp );

    while( !feof(fp) ) {
        if(fgets( line, MAX_LINE_LEN-1, fp )==NULL) printf("[NAplus Error] fgets in funct parse_GetDouble \n");

        /* get rid of white space */
        trim(line);

        /* is first character a comment ? */
        comment = is_comment_line( line );
        if( comment == _TRUE ) {   continue;  }

        match = key_matches( key, line );
        if( match == _FALSE ) {   continue;   }
#ifdef _LOG_PARSER
            printf("Line(%s) key=%s \n", line,key );
#endif

        /* strip word and equal sign */
        strip(line);

        double_val = strtod( line, NULL );
#ifdef _LOG_PARSER

        printf("Key(%s) -- Value(%e) \n", key, double_val );
#endif
        *value = double_val;
        *found = _TRUE;
        return;
    }

}
//-----------------------------------------------------------------------------
void parse_GetDoubleAllInstances( FILE *fp, const char key[], int *nvalues, double values[], int max_L, int *found )
{
    char line[MAX_LINE_LEN];
    int comment;
    int match;
    double double_val;
    int count;

    /* init */
    *found = _FALSE;
    count = 0;

    /* reset to start of file */
    rewind( fp );

    while( !feof(fp) ) {
        if(fgets( line, MAX_LINE_LEN-1, fp )==NULL) printf("[NAplus Error] fgets in funct parse_GetDoubleAllInstances \n");

        /* get rid of white space */
        trim(line);

        /* is first character a comment ? */
        comment = is_comment_line( line );
        if( comment == _TRUE ) {   continue;  }

        match = key_matches( key, line );
        if( match == _FALSE ) {   continue;   }


        /* strip word and equal sign */
        strip(line);

        double_val = strtod( line, NULL );

        if( max_L == count ) {
            printf("parse_GetDoubleAllInstances: Error, input array is not large enough to hold result \n");
            return;
        }
        values[count] = double_val;


        count++;

        *found = _TRUE;
    }

    if( *found == _TRUE ) {
        int i;

        printf("Key(%s) -- Values( %e", key, values[0] );
        for( i=1; i<count; i++ ) {
            printf(", %e", values[i] );
        }printf(" )\n");
    }


    *nvalues = count;

}
//-----------------------------------------------------------------------------
void parse_GetDoubleArray( FILE *fp, const char key[], int *nvalues, double values[], int *found )
{
    char line[MAX_LINE_LEN];
    int comment;
    int match;
    double double_val;
    char *_line;
    int count;


    /* init */
    *found = _FALSE;

    /* reset to start of file */
    rewind( fp );

    while( !feof(fp) ) {
        if(fgets( line, MAX_LINE_LEN-1, fp )==NULL) printf("[NAplus Error] fgets in funct parse_GetDoubleArray \n");

        /* get rid of white space */
        trim(line);

        /* is first character a comment ? */
        comment = is_comment_line( line );
        if( comment == _TRUE ) {   continue;  }

        match = key_matches( key, line );
        if( match == _FALSE ) {   continue;   }

        /* strip word and equal sign */
        strip(line);

        count = 0;
        _line = line;
        for(;;) {
            char * endp;

            double_val = strtod(_line, &endp);
            values[count] = double_val;

            if( endp == _line ) {
                break;
            }

            _line = endp;

            count++;
        }
#ifdef _LOG_PARSER
        printf("Key(%s) -- Values( %e", key, values[0] );
        int i;
        for( i=1; i<count; i++ ) {
            printf(", %e", values[i] );
        }printf(" )\n");
#endif

        *nvalues = count;
        *found = _TRUE;
        return;
    }

}
//-----------------------------------------------------------------------------
void parse_GetString( FILE *fp, const char key[], char value[], int max_L, int *found )
{
    char line[MAX_LINE_LEN];
    int comment;
    int match, line_L;
    char LINE[MAX_LINE_LEN];

    /* init */
    *found = _FALSE;

    memset( value, 0, sizeof(char)*max_L );

    /* reset to start of file */
    rewind( fp );


    while( !feof(fp) ) {
        if(fgets( line, MAX_LINE_LEN-1, fp )==NULL) printf("[NAplus Error] fgets in funct parse_GetString \n");

        /* get rid of white space */
        trim(line);

        /* is first character a comment ? */
        comment = is_comment_line( line );
        if( comment == _TRUE ) {   continue;  }

        match = key_matches( key, line );
        if( match == _FALSE ) {   continue;   }

        /* strip word and equal sign */
        strip(line);
        strip_all_whitespace(line, LINE);

        trim_past_comment(LINE);
        line_L = strlen( LINE );
        if( line_L > max_L ) {
            printf("parse_GetString: Error, input string is not large enough to hold result \n");
            return;
        }

        strncpy( value, LINE, line_L );
#ifdef _LOG_PARSER
        printf("Key(%s) -- Value(%s) \n", key, value );
#endif
        *found = _TRUE;
        return;
    }
}
