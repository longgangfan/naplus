#include "naplus.h"
NAplusContext naplus;
//-----------------------------------------------------------------------------
int main (int argc, char *argv[])
{
    MPI_Comm   opt_comm,fwrd_comm, host_comm;
    MPI_Group  opt_group,world_group;
    int        world_rank, *opt_global_rank,opt_cpu,world_size, shared_size,shared_rank;
    int        fwrd_group_size,fwrd_group_id,fwrd_group_num;
    int        k;
    time_t     rawtime;
    struct tm *timeinfo;
    const int  task_wait=0, task_start=1, task_stop=2;
    int        task=task_wait;
    double    *current_model=0,current_misfit=0;
    int        cntModels=0,nd=0;
    const int  num = 1;
    string     filename_stdout,strfileID,matlab_options;
    double     dum,t[2];

    // --- Initialize code ----------------------------------------------------
    // === MPI initialize ===
    MPI_Init (&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // time stamp
    time(&rawtime);
    timeinfo = localtime ( &rawtime );
    strftime(naplus.fileID,20,"%d%b%y_%H%M",timeinfo);
    strfileID=naplus.fileID;

#ifdef REDIRECT_STDOUT
    // redirect stdout to stdout-file
    filename_stdout = "STDOUT_NAplus_" + strfileID + ".txt";
    if(!world_rank) printf("[NAplus] You have set the PP directive -DREDIRECT_STDOUT: stdout is redirected to file %s \n",filename_stdout.c_str());
    freopen (filename_stdout.c_str(),"w", stdout);
#endif

    // print header
    if(!world_rank){
    printf("================================================================================\n");
    printf("                                === NAplus ===\n");
//    printf("Current Revision: %s - %s   \n",__SVNREVISION__,__SVNDATE__);
//    printf("Modified items: %s\n",__SVNMANCHANGES__);
    printf("Compiled: Date: %s - Time: %s - Mode:  %s       \n",__DATE__,__TIME__,__OPTMODE__);
    if(IS_LITTLE_ENDIAN){
            printf(" > Little endian system \n");
        }
        else{
            printf(" > Big endian system \n");
        }
    printf("================================================================================\n");
    }

    // check command line options
    check_commandline(MPI_COMM_WORLD,argc,argv,&fwrd_group_size,&fwrd_group_num);

    // load input file
    load_inputfile(MPI_COMM_WORLD);

    // --- Mpi communication --------------------------------------------------
    // (1) fwrd communication
    fwrd_group_id = (int) ((world_rank/fwrd_group_size)+1);
    MPI_Comm_split(MPI_COMM_WORLD,fwrd_group_id,world_rank,&fwrd_comm);
    
    // (2) opt communication
    opt_cpu = world_rank % fwrd_group_size;
    MPI_Comm_group(MPI_COMM_WORLD,&world_group);
    opt_global_rank = new int[fwrd_group_num];
    for(k=0;k<fwrd_group_num;k++)
        opt_global_rank[k] = k*fwrd_group_size;
    MPI_Group_incl(world_group,fwrd_group_num,opt_global_rank,&opt_group);
    delete [] opt_global_rank;
    MPI_Comm_create(MPI_COMM_WORLD,opt_group,&opt_comm);

    // --- NA / forward modeling ----------------------------------------------


#ifdef LaMEM
    // === PETSC initialize ===
    PetscErrorCode ierr;
    PETSC_COMM_WORLD = fwrd_comm;
    ierr = PetscInitialize(&argc,&argv,(char *)0,help_naplus); CHKERRQ(ierr);
#endif

#ifdef MATLAB
    // === MATLAB libraries initialize ===
    const char *pStrings[]={"-nojvm","-nojit","-nodisplay","-singleCompThread"};     // Always run MILAMIN  in -singleCompThread!

    // get extra communicator for shared memory regions
    //MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, world_rank, MPI_INFO_NULL, &host_comm); // can be used as soon MPI 3.0 standard is available
    MPI_Comm_split_host(MPI_COMM_WORLD, &host_comm);
    MPI_Comm_rank(host_comm, &shared_rank);
    MPI_Comm_size(host_comm, &shared_size);
    
    // Force all tasks on the same node to invoke MATLAB initialization routines in a consecutive manner to avoid MATLAB internal locking
    // this is  - of course - a bottleneck
    for( int i = 0; i < shared_size;i++)
    {
        if ( i == shared_rank )
        {
            t[0] = MPI_Wtime();
            if( !mclInitializeApplication(pStrings,4) )
            {
                fprintf(stderr, "[%d] Could not initialize the application.\n",world_rank);
            }
            else{
                printf("[%d] Initialize MATLAB application \n",world_rank);
            }
            
            if (!libmatlab_fwrdInitialize())
            {
                fprintf(stderr,"[%d] Could not initialize the library.\n",world_rank);
            }
            else
            {
                printf("[%d] Initialize MATLAB library \n",world_rank);
            }
            t[1] = MPI_Wtime();
            printf("[NAplus: %4d] Matlab init: %4.2f sec \n",world_rank,t[1]-t[0]);
        }
        MPI_Barrier( host_comm );
    }
    // free communicator
    MPI_Comm_free(&host_comm);

#endif



    // master->NA / slaves->wait for tasks
    if (opt_cpu == 0)
    {
        na(opt_comm,fwrd_comm);
        if(fwrd_group_size>1)
        {
            task=task_stop;
            communicate_task(fwrd_comm,&task);
        }
    }
    else{
        do{ communicate_task(fwrd_comm,&task);
            if(task == task_start)
                forward_LaMEM(nd,current_model,&current_misfit,fwrd_comm,opt_cpu,fwrd_group_id,cntModels,0,dum);
        }while(task != task_stop);
    }



#ifdef LaMEM
    // === PETSC finalize ===
    PetscFinalize();
#endif
    
#ifdef MATLAB
    // === MATLAB libraries finalize ===
    libmatlab_fwrdTerminate();
    mclTerminateApplication();
#endif
    
    

    // --- Finalize Code ------------------------------------------------------

    // deallocate global variables
    delete [] naplus.DensIdx;
    delete [] naplus.ViscIdx;

    // free groups & communicators
    MPI_Comm_free(&fwrd_comm);
    if(opt_comm!=MPI_COMM_NULL)
        MPI_Comm_free(&opt_comm);
    MPI_Group_free(&world_group);
    MPI_Group_free(&opt_group);

    printf("[rank %d] finished\n",world_rank);

    // === MPI finalize ===
    MPI_Finalize();

    return 0;
}
