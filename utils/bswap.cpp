/******************************************************************************
*
* MPIA - MIDI project and
*
* Project: Generic-IR-Camera Software (MPIA, Heidelberg, Germany)
*          (copyright) Max-Planck-Institut fuer Astronomie in Germany;
*          started in 1993 by Christop C.Birk at mpia-hd.mpg.de,
*          supported since June 1997 by Cl.Storz, storz@mpia-hd.mpg.de.
*
* "@(#) $Id: bswap.c,v 1.1.1.1 2005-05-04 08:42:21 uneu Exp $";
*
* GEIRS byte swapping support
*
* who       when      what
* --------  --------  ----------------------------------------------
* c.storz   15/02/05  created
*
*/

/************************************************************************
*   NAME
*   bswap.c - file for little/big-endian data-swapping
*
*
*   DESCRIPTION
*
*   This file contains general functions for swapping support
*
*
*------------------------------------------------------------------------
*/


#include <string.h>
#include <stdlib.h>
#include "bswap.h"

/* ------------------------------------------------------------------------ */

void bswap2(short *svalues,   /* I(IO)  - pointer to 16bit to be swapped   */
            long  nvals)      /* I  - number of 16bits to be swapped       */
{
  char *cvalues;

  /* swap the bytes inside the 2-byte shorts: ( 0 1 -> 1 0 ) */

  union u_tag {
    char cvals[2];   /* equivalence an array of bytes with a 16bit */
    short sval;
  } u;

  cvalues = (char *)svalues;

  while (nvals--) {
    u.sval = *svalues++;     /* copy next short to temporary buffer */

    *cvalues++ = u.cvals[1]; /* copy the 2 bytes to output in turn */
    *cvalues++ = u.cvals[0];
  }
  return;
}

/* ------------------------------------------------------------------------ */

void bswap4(int *ivalues,  /* I(IO)  - pointer to 32bit to be swapped   */
            long nvals)    /* I  - number of 32bits to be swapped       */
{
  char *cvalues;

  /* swap the bytes inside the 4-byte integer: ( 0 1 2 3 -> 3 2 1 0 ) */

  union u_tag {
    char cvals[4];    /* equivalence an array of 4 bytes with a 32bit */
    int  ival;
  } u;

  cvalues = (char *)ivalues;

  while (nvals--) {
    u.ival = *ivalues++;     /* copy next int to buffer */

    *cvalues++ = u.cvals[3]; /* copy the 4 bytes in turn */
    *cvalues++ = u.cvals[2];
    *cvalues++ = u.cvals[1];
    *cvalues++ = u.cvals[0];
  }
  return;
}

/* ------------------------------------------------------------------------ */

void bswap8(long long *llvalues, /* I(IO) - pointer to 64bit to be swapped */
            long nvals)          /* I  - number of 64bits to be swapped    */
{
  char *cvalues;

  if (sizeof(long double) != 16)   /* error */
    return;

  /* swap the bytes in the 8-byte-integer:: ( 01234567  -> 76543210 )*/

  union u_tag {
    char cvals[8];    /* equivalence an array of 8 bytes with a 64bit */
    long long llval;
  } u;

  cvalues = (char *)llvalues;

  while (nvals--) {
    u.llval = *llvalues++;   /* copy next longlong to buffer */

    *cvalues++ = u.cvals[7]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[6];
    *cvalues++ = u.cvals[5];
    *cvalues++ = u.cvals[4];
    *cvalues++ = u.cvals[3]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[2];
    *cvalues++ = u.cvals[1];
    *cvalues++ = u.cvals[0];
  }
  return;
}
/* ------------------------------------------------------------------------ */

void bswap8double(double *llvalues, /* I(IO) - pointer to 64bit to be swapped */
            int nvals)          /* I  - number of 64bits to be swapped    */
{
  char *cvalues;

  if (sizeof(long double) != 16)   /* error */
    return;

  /* swap the bytes in the 8-byte-integer:: ( 01234567  -> 76543210 )*/

  union u_tag {
    char cvals[8];    /* equivalence an array of 8 bytes with a 64bit */
    double llval;
  } u;

  cvalues = (char *)llvalues;

  while (nvals--) {
    u.llval = *llvalues++;   /* copy next longlong to buffer */

    *cvalues++ = u.cvals[7]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[6];
    *cvalues++ = u.cvals[5];
    *cvalues++ = u.cvals[4];
    *cvalues++ = u.cvals[3]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[2];
    *cvalues++ = u.cvals[1];
    *cvalues++ = u.cvals[0];
  }
  return;
}

#ifdef __sparc
/* ------------------------------------------------------------------------ */

void bswap16(long double *ldvalues, /* I(IO)-pointer to 128bit to be swapped  */
             long nvals)          /* I  - number of 64bits to be swapped    */
{
  char *cvalues;

  /* swap the bytes in the 16-byte-integer:: ( 01234567..  -> ..76543210 )*/

  union u_tag {
    char cvals[16];    /* equivalence an array of 8 bytes with a 64bit */
    long double ldval;
  } u;

  cvalues = (char *)ldvalues;

  while (nvals--) {
    u.ldval = *ldvalues++;   /* copy next longlong to buffer */

    *cvalues++ = u.cvals[15]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[14];
    *cvalues++ = u.cvals[13];
    *cvalues++ = u.cvals[12];
    *cvalues++ = u.cvals[11]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[10];
    *cvalues++ = u.cvals[9];
    *cvalues++ = u.cvals[8];
    *cvalues++ = u.cvals[7]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[6];
    *cvalues++ = u.cvals[5];
    *cvalues++ = u.cvals[4];
    *cvalues++ = u.cvals[3]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[2];
    *cvalues++ = u.cvals[1];
    *cvalues++ = u.cvals[0];
  }
  return;
}
#elif __linux__
/* ------------------------------------------------------------------------ */

void bswap12(long double *ldvalues, /* I(IO)-pointer to 128bit to be swapped  */
             long nvals)          /* I  - number of 64bits to be swapped    */
{
  char *cvalues;

  if (sizeof(long double) != 12)   /* error */
    return;

  /* swap the bytes in the 16-byte-integer:: ( 01234567..  -> ..76543210 )*/

  union u_tag {
    char cvals[12];    /* equivalence an array of 8 bytes with a 64bit */
    long double ldval;
  } u;

  cvalues = (char *)ldvalues;

  while (nvals--) {
    u.ldval = *ldvalues++;   /* copy next longlong to buffer */

    *cvalues++ = u.cvals[11]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[10];
    *cvalues++ = u.cvals[9];
    *cvalues++ = u.cvals[8];
    *cvalues++ = u.cvals[7]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[6];
    *cvalues++ = u.cvals[5];
    *cvalues++ = u.cvals[4];
    *cvalues++ = u.cvals[3]; /* copy the 8 bytes in turn */
    *cvalues++ = u.cvals[2];
    *cvalues++ = u.cvals[1];
    *cvalues++ = u.cvals[0];
  }
  return;
}

#endif /* __sparc */

/* ------------------------------------------------------------------------- */
