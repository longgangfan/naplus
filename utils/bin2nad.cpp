#include "bin2nad.h"
// ============================================================================
int main (int argc, char *argv[])
{
	int rank;
	char *prefix;
	
		
	check_commandline(argc,argv,&rank,&prefix);
	bin2ascii_main(rank,prefix);
	free(prefix);

	return 0;
}
// ============================================================================
void check_commandline(int argc,char *argv[],int *rank,char **prefix)
{
	// argv[0] == name of program
	if(argc<2){
		printf("[bin2nad error] Not enough command line arguments.\n");
		exit(1);		
	}
	else if(argc==2){
		if(strcmp(argv[1],"-help")==0){
			printf("%s",help);
			exit(1);
		}
		else{
			printf("[bin2nad error] Not enough command line arguments.\n");
			exit(1);
		}
	}
	else if(argc==5){
		if(strcmp(argv[1],"-rank")==0 && strcmp(argv[3],"-prefix")==0){
			*rank = atoi(argv[2]);
			if(asprintf(prefix,"%s",argv[4])<0) printf("[NAplus Error] asprintf in funct check_commandline");
			
			if(strcmp(*prefix,"own")!=0 && strcmp(*prefix,"all")!=0 && strcmp(*prefix,"merge")!=0){
				printf("[bin2nad error] Unknown prefix.\n");
				printf("[bin2nad error] prefix: %s\n",*prefix);
				exit(1);
			}
		}
		else{
			printf("[bin2nad error] Unknown command line arguments.\n");
			printf("[bin2nad error] -rank ,-Ndim, -prefix should be used.\n");
			exit(1);		
		}
	}
	else{
		printf("[bin2nad error] Wrong number of command line arguments: %d.\n",argc);
		exit(1);
	}
	return;
}
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
void bin2ascii_main(int rank, char *prefix){
/* format:
 * | cntModels | cntModels_own | source | param1 | param2 | ... | paramN | misfitval | ctime |
 *
 */
	FILE   *fileBIN;
	double *buffer,*ranges_in;
	float  *models,*mfits,*rngs;
	char   *filename;
	int     filesize;
	int     Nbuffer,Ndim;
	int     position,Nmodels,headersize,n,k;
	int     restart,opt_rank,opt_size,Nheader;


	// open binary to read
	if(strcmp(prefix,"merge")==0){
		if(asprintf(&filename,"models_%s.bin",prefix)<0) printf("[NAplus Error] asprintf in funct bin2ascii_main");
	}
	else{
		if(asprintf(&filename,"models_%s_ID%d.bin",prefix,rank)<0) printf("[NAplus Error] asprintf in funct bin2ascii_main");
	}
	fileBIN = fopen(filename,"r");
	if (fileBIN == NULL) {
  		fprintf(stderr, "Can't open file: %s \n",filename);
  		exit(1);
  	}
	free(filename);
	
	

	// obtain binary file size
	fseek (fileBIN , 0 , SEEK_END);
	filesize = (int) ftell(fileBIN);
	rewind (fileBIN);

	// Read header information (there is no need for opt_rank and opt_size yet)
	position = 0;
	Nheader = 4;
	read_array(&restart,1,&position,fileBIN);
	read_array(&opt_rank,1,&position,fileBIN);
	read_array(&opt_size,1,&position,fileBIN);
	read_array(&Ndim,1,&position,fileBIN);
	ranges_in = new double[2*Ndim];
	read_array(ranges_in,2*Ndim,&position,fileBIN);
	headersize = Nheader*(int)sizeof(int) + 2*Ndim*(int)sizeof(double);

	// =============
	Nbuffer=Ndim+5;
	// =============

	// determine the number of models (all are of double precision)
	Nmodels = (filesize-headersize)/(int)sizeof(double)/(Nbuffer);
	printf("> number of models: %d\n",Nmodels);

	// allocate buffer to read a full model + misfit + modelID
	buffer = new double[Nbuffer];
	models = new float[Nmodels*Ndim];
	mfits  = new float[Nmodels];
	rngs   = new float[2*Ndim];

	for(k=0;k<(2*Ndim);k++)
		rngs[k] = ranges_in[k];

	// read binary data
	n = 0;
	while(position<filesize){
		read_array(buffer,Nbuffer,&position,fileBIN);

		for(k=3;k<=Ndim+2;k++)
			models[(n*Ndim)+(k-3)] = (float) buffer[k];
		mfits[n] = (float) buffer[Ndim+3];
		n=n+1;
	}


	// write nad file
	FSUB(write_nad_file)(&Ndim,&Nmodels,rngs,models,mfits);



	// deallocate buffer
	delete[] buffer;
	delete[] ranges_in;
	delete[] models;
	delete[] mfits;
	delete[] rngs;

	// close files
	fclose(fileBIN);
	
	return;
}
