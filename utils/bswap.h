/***************************************************************************
*
* MPIA - MIDI project and
*
* Project: Generic-IR-Camera Software (MPIA, Heidelberg, Germany)
*          (copyright) Max-Planck-Institut fuer Astronomie in Germany;
*          started in 1993 by Christop C.Birk at mpia-hd.mpg.de,
*          supported since June 1997 by Cl.Storz, storz@mpia-hd.mpg.de.
*
* "@(#) $Id: bswap.h,v 1.1.1.1 2005-05-04 08:42:21 uneu Exp $";
*
* GEIRS byte swapping support
*
* who       when      what
* --------  --------  ----------------------------------------------
* c.storz   15/02/05  created
*
*/

/************************************************************************
*   NAME
*   bswap.h - file for little/big-endian data-swapping
*
*   This file contains function declarations for swapping support
*
*------------------------------------------------------------------------
*/

#ifndef INCLUDE_BSWAP_H
#define INCLUDE_BSWAP_H

void bswap2(short *svalues,   /* I(IO)  - pointer to 16bit to be swapped   */
            long  nvals);     /* I  - number of 16bits to be swapped       */

void bswap4(int *ivalues,     /* I(IO)  - pointer to 32bit to be swapped   */
            long nvals);      /* I  - number of 32bits to be swapped       */

void bswap8(long long *llvalues, /* I(IO) - pointer to 64bit to be swapped */
            long nvals);         /* I  - number of 64bits to be swapped    */

void bswap8double(double *llvalues, /* I(IO) - pointer to 64bit to be swapped */
            int nvals) ;
#ifdef __sparc
void bswap16(long double *ldvalues, /* I(IO)-pointer to 128bit to be swapped  */
             long nvals);           /* I  - number of 64bits to be swapped    */
#elif __linux__
void bswap12(long double *ldvalues, /* I(IO)-pointer to 128bit to be swapped  */
             long nvals);           /* I  - number of 64bits to be swapped    */
#endif /* __sparc */

/* ------------------------------------------------------------------------- */

#endif  /* INCLUDE_BSWAP_H */

/* ------------------------------------------------------------------------- */
