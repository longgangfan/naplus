! ----------------------------------------------------------------------------
! 
!       Driver program that calls the Neighbourhood algorithm 
!     ensemble inference routines to calculate Bayesian 
!     information measures from an input ensemble of models.
! 
! 
!                 MEMORY AND ARRAY SIZES
! 
!                                       The NAB routines use the
!                                       include file 'nab_param.inc' 
!                             to define all parameters controlling
!                                       memory required by the arrays.
! 
!                                       A description of each parameter,
!                                       indicating which ones should be
!                                       changed to fit your application
!                                       can be found in 'nab_param.inc'
! 
! 
!                 INPUT AND OUTPUT
! 
!       Input files:
!                   nab.in              Contains options for Neighbourhood
!                                       algorithm integration (see HTML manual)
! 
!               ensemble.nad  Direct access NAD file contain
!                             input models and misfit values
! 
!       Output files:
!                   nab.sum             summary of results
! 
!                   nab.out             output file containing results of
!                             all integrals and marginals
!                             (This file is read by plot programs
!                              `naplot-x, naplot-p') 
! 
!                   sobol.coeff         initializing data used for
!                                       quasi-random sequences
!                                       (This is output for reference only)
! 
!       Comments:
! 
!                Logical units 30-40 are reserved for use by NAB subroutines
!                The user specific routines should not use these values
!                for logical units. The NAB routines also write to 
!            LU 6 as standard out.
! 
!                                               M. Sambridge, RSES, ANU.
!                                               Last updated Sept. 1999.
! 
! ----------------------------------------------------------------------------
!                             
        Program gauss_nab
!                                       Call NAB routine to do the work

      call nab

      stop
      end
! 
! ---------------------------------------------------------------------------
! 
!     user_init - user supplied routine for any initialization 
!               tasks needed for user supplied problem.
! 
!            THIS IS A PROBLEM SPECIFIC USER SUPPLIED SUBROUTINE
! 
!       Input:
! 
!       Output:
! 
!     Comments:
!            This routine is called once at the start of the
!            nab program to perform any initialization tasks
!            that may be needed. For example, calculate a priori
!            probability density functions, read in data etc.
!            If no tasks are required then a simple dummy routine
!            may be used.
! 
!                                       M. Sambridge, RSES, July 1998.
! 
! ---------------------------------------------------------------------------
! 
        Subroutine user_init()

        parameter (ndmax=100)

        real*4        xc(ndmax)
        real*4        invcov(ndmax,ndmax)
        real*4        ident(ndmax,ndmax)
        integer       ndof,nd

        common /userinit1/nd,ndof,xc,invcov
!                                   read number of variables

        lu = 90
         open(lu,file='dof.in',status='old')        
         read(lu,*) ndof
         close(lu)
         
         write(*,*)'---------------------------------------'
         write(*,*)'Number of degree of freedom: ',ndof
         write(*,*)'---------------------------------------'
                  

           lu = 90
           open(lu,file='gauss.in',status='old')
           do i=1,9
              read(lu,*)
           end do
!           read(lu,fmt="(34x,i10)")nd
            read(lu,*) nd
            write(*,*)'Number of dimensions',nd
            
! 
!                                   check array sizes
           if(nd.gt.ndmax)then
             write(*,*)
             write(*,*)'Error in subroutine user_init'
             write(*,*) &
     &       '        maxmimum number of dimensions is too small'
             write(*,*) &
     &       '        current maximum number of dimensions =',ndmax
             write(*,*) &
     &       '        number of dimensions in input file   =',nd
             write(*,*)'        '
             write(*,*)'Remedy - increase size of parameter ndmax'
             write(*,*)'         to at least this value and recompile'
             write(*,*)'        '
             stop
           end if
!                                   read in inverse covariance
!                                   of Gaussian
           do i=1,6
              read(lu,*)
           end do
           read(lu,*)iformat
           do i=1,ndmax
           do j=1,ndmax
              invcov(i,j) = 0.0
              ident(i,j) = 0.0
           end do
           end do
           do i=1,nd
              ident(i,i) = 1.0
           end do
           if(iformat.le.0)then
!                                   read in covariance matrix
!                                   as diagonals
!              read(lu,*)(invcov(i,i),i=1,nd)
              do i=1,nd
                read(lu,*) invcov(i,i)
               end do
              
              do i=1,nd
                 if(invcov(i,i).eq.0)then
                    write(*,*)' Covariance matrix read in from gauss.in'
                    write(*,*)' has zero eigenvalues can not calculate '
                    write(*,*)' inverse'
                    stop
                 end if
                 invcov(i,i) = 1./invcov(i,i)
              end do
           else if(iformat.eq.1)then
!                                   read in full covariance matrix
           do i=1,nd
              read(lu,*)
              read(lu,*)(invcov(i,j),j=1,nd)
           end do
!                                   check symmetry
           do i=1,nd
           do j=1,nd
              if(invcov(i,j).ne.invcov(j,i))then
                write(*,*)
                write(*,*)' Error - inverse covariance matrix read'
                write(*,*)' in from file gauss.in is not symmetric'
                write(*,*)
                write(*,*)' Entry ', i,j,' : ',invcov(i,j),invcov(j,i)
                write(*,*)
                stop
              end if
           end do
           end do
! 
!                                               calculate inverse covariance matrix
           call gaussj(invcov,nd,ndmax,ident,nd,ndmax)

           else if(iformat.gt.1)then
!                                   read in full inverse covariance matrix
           do i=1,nd
              read(lu,*)
              read(lu,*)(invcov(i,j),j=1,nd)
           end do
!                                   check symmetry
           do i=1,nd
           do j=1,nd
              if(invcov(i,j).ne.invcov(j,i))then
                write(*,*)
                write(*,*)' Error - inverse covariance matrix read'
                write(*,*)' in from file gauss.in is not symmetric'
                write(*,*)
                write(*,*)' Entry ', i,j,' : ',invcov(i,j),invcov(j,i)
                write(*,*)
                stop
              end if
           end do
           end do
           end if
!                                   read in centre of Gaussian
           read(lu,*)
           read(lu,*)
!           read(lu,*)(xc(i),i=1,nd)
              do i=1,nd
                read(lu,*) xc(i)
                write(*,*) xc(i)
               end do
           close(lu)



        return
        end
! 
! ---------------------------------------------------------------------------
! 
!     logPPD_NA - calculates log-PPD of posterior probability 
!               density function for user supplied problem using the
!                 Neighbourhood approximation to the PPD.
! 
!            THIS IS A PROBLEM SPECIFIC USER SUPPLIED SUBROUTINE
! 
!       Input:
!           node        : index of model from NAD input file
!           misfit(*)         : array of input data (misfit values)
! 
!       Output:
!           logPPD_NA   : log-posterior probability density
!                   function of model
! 
!     Comments:
!            This routine converts the input data array to a
!            log-posteriori probability density function. 
!            It is a user supplied routine.
! 
!            For example if the input data for each model is
!            a simple sum of squares of residuals weighted
!            by a priori data covariances (i.e. standard least squares)
!            then the Log-PPD is just a factor of -0.5 times
!            this. This routine allows the user to use other
!            Likelihood functions (or forms of posteriori probability
!            density) and if necessary rescale them, or include 
!            a priori PDFs. 
! 
!            WARNING: All Bayesian integrals will be affected
!            by the choice and scale of this function ! The weight
!            of each model in the integrals depends on the 
!            exponential of logPPD. It is very important that
!            the data covariances are chosen suitably for the user problem. 
! 
! 
!                                       M. Sambridge, RSES, July 1998.
! 
! ---------------------------------------------------------------------------
! 
        Function logPPD_NA(node,misfit)

       real*4           misfit(*)
       real*4           logPPD_NA
       integer      nd,ndof
       parameter (ndmax=100)
       real*4        xc(ndmax)
       real*4        invcov(ndmax,ndmax)

       common /userinit1/nd,ndof,xc,invcov


       logPPD_NA = -0.5*real(ndof)*misfit(node)

       return
       end

! ---------------------------------------------------------------------------
! 
       Function logL_NA(node,misfit)

       real*4           misfit(*)
       real*4           logL_NA
       parameter (ndmax=100)
       real*4        xc(ndmax)
       real*4        invcov(ndmax,ndmax)
       integer       ndof
       integer       nd

       common /userinit1/nd,ndof,xc,invcov


       logL_NA = 1.0
!       logL_NA = -0.5*real(ndof)*misfit(node)

       return
       end

! ---------------------------------------------------------------------------
! 
        Function gasdev(idum)
        integer  idum
        real*4   gasdev
        integer  iset
        real*4   fac,gset,rsq,v1,v2,ran3
        save iset,gset
        DATA iset/0/
        if(idum.lt.0) iset=0
        if(iset.eq.0) then
 1         v1=2.0*ran3(idum)-1.0
           v2=2.0*ran3(idum)-1.0
           rsq=v1**2+v2**2
           if(rsq.ge.1.0.or.rsq.eq.0.0)goto 1
           fac=sqrt(-2.0*log(rsq)/rsq)
           gset=v1*fac
           gasdev=v2*fac
        else
           gasdev=gset
           iset=0
        endif
        return
        end
! 
! ---------------------------------------------------------------------------
! 
!     logPPD - calculates log-PPD of posterior probability 
!            density function for user supplied problem.
! 
!            In this case a simple Gaussian distribution 
!            in 24 dimensions.
! 
!            THIS IS A PROBLEM SPECIFIC USER SUPPLIED SUBROUTINE
! 
!       Input:
!           x           : model at which logPPD is to be evaluated
! 
!       Output:
!           logPPD      : log-posterior probability density
!                   function of input model
! 
!     Comments:
!              This routine was written to allow the program to 
!              be used for evaluation of Bayesian integrals where the
!              forward problem IS to be called. i.e. here we do not use the
!              neighbourhood approximation to the forward problem
!              but instead solve the actual forward problem
! 
!              Definition of the logPPD:
! 
!            An example: If the input data for each model is
!            a simple sum of squares of residuals weighted
!            by a priori data covariances (i.e. standard least squares)
!            then the Log-PPD is just a factor of -0.5 times
!            this. This routine allows the user to use other
!            Likelihood functions (or forms of posteriori probability
!            density) and if necessary rescale them, or include 
!            a priori PDFs. 
! 
!            WARNING: All Bayesian integrals will be affected
!            by the choice and scale of this function ! The weight
!            of each model in the integrals depends on the 
!            exponential of logPPD. It is very important that
!            the data covariances are chosen suitably for the user problem. 
! 
!      
!                                       M. Sambridge, RSES, April 2005.
! 
! ---------------------------------------------------------------------------
! 
        Function logPPD(x)

        real*4            x(*)
        real*4            logPPD
        parameter (ndmax=100)

        real*4        xc(ndmax)
        real*4        res(ndmax)
        real*4        invcov(ndmax,ndmax)

        common /userinit1/nd,ndof,xc,invcov


!                               In this routine we set up a test PPD 
!                               which is a multi-dimensional Gaussian           
! 
!            NOTE: For this simple test problem the expected values 
!            and the covariance matrix are set here and can be 
!            compared to the results of the Bayesian integrals
!            for the same quantities. HOWEVER since the integration 
!            is performed over a finite size box, the expectations
!            and covariances will only be the same if the box width
!            is large compared to the co-variances. Otherwise the
!            actual PDF integrated will be a `curtailed' version of
!            this Gaussian with slighly different means and variances.
! 
        d = 0.0
        do i=1,nd
           res(i) = x(i)-xc(i)
        end do
        do i=1,nd
        do j=1,nd
           d = d +  res(i)*invcov(i,j)*res(j)
        end do
        end do
        logPPD = -0.5*d
!       write(*,*)' logPPD ',logPPD

      return
      end
! 
! ---------------------------------------------------------------------------
! 
!     evaluate_sp - evaluates the i-th user supplied special function of 
!               the model for numerical intergation.
! 
!            THIS IS A PROBLEM SPECIFIC USER SUPPLIED SUBROUTINE
! 
!     Input:
!           i                 :index of special function to evaluate
!           x(nd)       :model values in raw  (input) units
!           nd          :number of dimensions
!           node        :node of Voronoi cell containing x.
! 
!     Output:
!           spf         :value of i-th special function
! 
!       Comments:
!            This routine is for the receiver function 
!            parameterisation used by Shibutani et al. (1997).
! 
!            Parameter 1  = velocity jump across Moho
!            Parameter 2  = depth to Moho
!            Parameter 3  = velocity gradient in first layer
!            Parameter 4  = velocity gradient in second layer
!            Parameter 5  = velocity gradient in third layer
!            Parameter 6  = velocity gradient in fourth layer
!            Parameter 7  = velocity gradient in fifth layer
!            Parameter 8  = depth of first interface 
!            Parameter 9  = depth of second interface 
!            Parameter 10 = depth of third interface 
!            Parameter 11 = depth of fourth interface 
!            Parameter 12 = depth of fifth interface 
! 
!                                               M. Sambridge, March 1998.
! 
! ---------------------------------------------------------------------------
! 
        Subroutine evaluate_sp (i,x,nd,node,spf)
! 
      real*4            x(nd)

      if(i.eq.1)then
!                                   evaluate DVmoho 
         spf = x(12)-x(17)

      else if(i.eq.2)then
!                                   evaluate depth to Moho 

         spf = x(1)+x(2)+x(3)+x(4)+x(5)

      else if(i.eq.3)then
         t = x(1)
         if(t.eq.0.0)t = 0.1
         spf = (x(13)-x(7))/t
      else if(i.eq.4)then
         t = x(2)
         if(t.eq.0.0)t = 0.1
         spf = (x(14)-x(8))/t
      else if(i.eq.5)then
         t = x(3)
         if(t.eq.0.0)t = 0.1
         spf = (x(15)-x(9))/t
      else if(i.eq.6)then
         t = x(4)
         if(t.eq.0.0)t = 0.1
         spf = (x(16)-x(10))/t
      else if(i.eq.7)then
         t = x(5)
         if(t.eq.0.0)t = 0.1
         spf = (x(17)-x(11))/t
      else if(i.eq.8)then
         spf = x(1)
      else if(i.eq.9)then
         spf = x(1)+x(2)
      else if(i.eq.10)then
         spf = x(1)+x(2)+x(3)
      else if(i.eq.11)then
         spf = x(1)+x(2)+x(3)+x(4)
      else if(i.eq.12)then
         spf = x(1)+x(2)+x(3)+x(4)+x(5)
      else
      end if

 100    format(/'  *****  Error in subroutine evaluate_sp  *****'/&
     &          '  Number of special functions supplied =',&
     &          i4,/'  Number requested =',i4//&
     &          '  Check number of special functions in input file'/)

      return
      end
! 
! ---------------------------------------------------------------------------
! 
!     evaluate_sp_range - returns the range of the i-th user supplied
!                   special function 
! 
!            THIS IS A PROBLEM SPECIFIC USER SUPPLIED SUBROUTINE
! 
!     Input:
!           i                 :index of special function to evaluate
!           x(nd)       :model values in raw  (input) units
!           nd          :number of dimensions
!           node        :node of Voronoi cell containing x.
! 
!     Output:
!           spmax       :maximum value of i-th special function
!           spmin       :minimum value of i-th special function
! 
!       Comments:
!            This routine is for the receiver function 
!            parameterisation used by Shibutani et al. (1997).
! 
!            Parameter 1 = velocity jump across Moho
!            Parameter 2 = depth to Moho
!            Parameter 3  = velocity gradient in first layer
!            Parameter 4  = velocity gradient in second layer
!            Parameter 5  = velocity gradient in third layer
!            Parameter 6  = velocity gradient in fourth layer
!            Parameter 7  = velocity gradient in fifth layer
!            Parameter 8  = depth of first interface 
!            Parameter 9  = depth of second interface 
!            Parameter 10 = depth of third interface 
!            Parameter 11 = depth of fourth interface 
!            Parameter 12 = depth of fifth interface 
! 
!                                               M. Sambridge, March 1998.
! 
! ---------------------------------------------------------------------------
! 
        Subroutine evaluate_sp_range(i,range,spmax,spmin)
! 
      real*4            range(2,*)

      if(i.eq.1)then
!                                   evaluate DVmoho range
         spmin = range(1,12)-range(2,17)
         spmax = range(2,12)-range(1,17)

      else if(i.eq.2)then
!                                   evaluate Moho range

         spmin = range(1,1)+range(1,2)+range(1,3)&
     &             +range(1,4)+range(1,5)
         spmax = range(2,1)+range(2,2)+range(2,3)&
     &             +range(2,4)+range(2,5)

      else if(i.eq.3)then
         t = range(1,1)
         if(t.eq.0.0)t = 0.1
         spmax = (range(2,7)-range(1,13))/t
         if(range(1,13).lt.range(2,7))then
            spmin = (range(1,13)-range(2,7))/t
         else
            spmin = (range(1,13)-range(2,7))/range(2,1)
         end if
      else if(i.eq.4)then
         t = range(1,2)
         if(t.eq.0.0)t = 0.1
         spmax = (range(2,14)-range(1,8))/t
         if(range(1,14).lt.range(2,8))then
            spmin = (range(1,14)-range(2,8))/t
         else
            spmin = (range(1,14)-range(2,8))/range(2,2)
         end if
      else if(i.eq.5)then
         spmax = (range(2,15)-range(1,9))/range(1,3)
         if(range(1,15).lt.range(2,9))then
            t = range(1,3)
            if(t.eq.0.0)t = 0.1
            spmin = (range(1,15)-range(2,9))/t
         else
            spmin = (range(1,15)-range(2,9))/range(2,3)
         end if
      else if(i.eq.6)then
         spmax = (range(2,16)-range(1,10))/range(1,4)
         if(range(1,16).lt.range(2,10))then
            t = range(1,4)
            if(t.eq.0.0)t = 0.1
            spmin = (range(1,16)-range(2,10))/t
         else
            spmin = (range(1,16)-range(2,10))/range(2,4)
         end if
      else if(i.eq.7)then
         spmax = (range(2,17)-range(1,11))/range(1,5)
         if(range(1,17).lt.range(2,11))then
            t = range(1,5)
            if(t.eq.0.0)t = 0.1
            spmin = (range(1,17)-range(2,11))/t
         else
            spmin = (range(1,17)-range(2,11))/range(2,5)
         end if
      else if(i.eq.8)then
         spmin = range(1,1)
         spmax = range(2,1)
      else if(i.eq.9)then
         spmin = range(1,1)+range(1,2)
         spmax = range(2,1)+range(2,2)
      else if(i.eq.10)then
         spmin = range(1,1)+range(1,2)+range(1,3)
         spmax = range(2,1)+range(2,2)+range(2,3)
      else if(i.eq.11)then
         spmin = range(1,1)+range(1,2)+range(1,3)&
     &             +range(1,4)
         spmax = range(2,1)+range(2,2)+range(2,3)&
     &             +range(2,4)
      else if(i.eq.12)then
         spmin = range(1,1)+range(1,2)+range(1,3)&
     &             +range(1,4)+range(1,5)
         spmax = range(2,1)+range(2,2)+range(2,3)&
     &             +range(2,4)+range(2,5)

      end if

      return
      end
      SUBROUTINE gaussj(a,n,np,b,m,mp)
      INTEGER m,mp,n,np,NMAX
      REAL a(np,np),b(np,mp)
      PARAMETER (NMAX=50)
      INTEGER i,icol,irow,j,k,l,ll,indxc(NMAX),indxr(NMAX),ipiv(NMAX)
      REAL big,dum,pivinv
      do 11 j=1,n
        ipiv(j)=0
11    continue
      do 22 i=1,n
        big=0.
        do 13 j=1,n
          if(ipiv(j).ne.1)then
            do 12 k=1,n
              if (ipiv(k).eq.0) then
                if (abs(a(j,k)).ge.big)then
                  big=abs(a(j,k))
                  irow=j
                  icol=k
                endif
              else if (ipiv(k).gt.1) then
                write (*,*) 'singular matrix in gaussj'
              endif
12          continue
          endif
13      continue
        ipiv(icol)=ipiv(icol)+1
        if (irow.ne.icol) then
          do 14 l=1,n
            dum=a(irow,l)
            a(irow,l)=a(icol,l)
            a(icol,l)=dum
14        continue
          do 15 l=1,m
            dum=b(irow,l)
            b(irow,l)=b(icol,l)
            b(icol,l)=dum
15        continue
        endif
        indxr(i)=irow
        indxc(i)=icol
        if (a(icol,icol).eq.0.) write(*,*) 'singular matrix in gaussj'
        pivinv=1./a(icol,icol)
        a(icol,icol)=1.
        do 16 l=1,n
          a(icol,l)=a(icol,l)*pivinv
16      continue
        do 17 l=1,m
          b(icol,l)=b(icol,l)*pivinv
17      continue
        do 21 ll=1,n
          if(ll.ne.icol)then
            dum=a(ll,icol)
            a(ll,icol)=0.
            do 18 l=1,n
              a(ll,l)=a(ll,l)-a(icol,l)*dum
18          continue
            do 19 l=1,m
              b(ll,l)=b(ll,l)-b(icol,l)*dum
19          continue
          endif
21      continue
22    continue
      do 24 l=n,1,-1
        if(indxr(l).ne.indxc(l))then
          do 23 k=1,n
            dum=a(k,indxr(l))
            a(k,indxr(l))=a(k,indxc(l))
            a(k,indxc(l))=dum
23        continue
        endif
24    continue
      return
      END
!  (C) Copr. 1986-92 Numerical Recipes Software '%1&9p#!.
 