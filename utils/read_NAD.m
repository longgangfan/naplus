
function [result] = read_NAD(filename)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   
%   [result] = read_NAD(filename)
%
%   reads Neighbouhood Algorithm outputfiles (ASCII) and creates a matlab
%   struct
%   
%
%   Tobias Baumann, May 2011, ETH Zuerich 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
disp('-----------  This is a beta version  -----------');
disp('-------------- check your results --------------');

% errorchecking
if ~ischar(filename)
    error('input must be a string')
end

NAfile = dir(filename);

if isempty(NAfile)
    disp(['There is no file: ' filename]);
    disp('Here are some alternatives: ');
    dir(['*' filename '*']);
    error('Please choose a correct filename');
else
    filename = NAfile.name;
end




%% GENERAL
tic;
% read format and scan for header keywords
fid = fopen(filename,'r'); 
    InputText=textscan(fid,'%s','delimiter','\n'); 
    [~,end_header]  = max(cellfun(@length,strfind(InputText{1},'Model and data values')));
    InputText       = [InputText{:}];

    result.filename = filename;
    result.header   = InputText(1:end_header-1);
    result.datatext = InputText(end_header+2:end);
fclose(fid);

% save datatext as separated text
fidh1 = fopen(['header_' filename '.tmp'],'wt');
    fprintf(fidh1,'%s\n',result.header{:});
fclose(fidh1);

%% HEADER

% load headertext
fidh2 = fopen(['header_' filename '.tmp'],'r');
     header= textscan(fidh2,'%s');
fclose(fidh2);

% add header elements to the result struct
result.nadname          = header{1}{7};
result.numofvar         = str2double(header{1}{8});
result.numofmod         = str2double(header{1}{13});
result.numof1stit       = str2double(header{1}{27});
result.numofsampsperit  = str2double(header{1}{35});    
result.numofcells2resamp= str2double(header{1}{43});
result.numofruns        = (result.numofmod - result.numof1stit) / result.numofsampsperit;



% read the parameter ranges
ind = 61; % index of the first parameter
for k = 1:result.numofvar
    result.paramLB(k) = str2double(header{1}{ind  });
    result.paramRB(k) = str2double(header{1}{ind+1});
    ind = ind+4;
end

%% DATA 

% save datatext as separated text
fid2 = fopen(['data_' filename '.tmp'],'wt');
    fprintf(fid2,'%s\n',result.datatext{:});
fclose(fid2);

% generate a key to read the datatext-columns
textkey = '%f';
for k = 1:(result.numofvar+1)
    textkey = [textkey ' %f'];
end

% read textcolumns
fid3 = fopen(['data_' filename '.tmp'],'r');
    data = textscan(fid,textkey);% fid3 or fid ???????
fclose(fid3);


% write datacolumns into a struct
result.key                      = data{1  };
for k = 1:(result.numofvar)
    result.parameter(:,k)       = data{k+1};
end
result.misfit                   = data{end};

% get the minimum misfit
[result.min_val,result.min_ind] = min(result.misfit);

%% delete temporary file
delete(['*_' filename '.tmp']);
disp(' ');
toc
disp(' ');
disp('------------------------------------------------');
end
% END OF FUNCTION =========================================================