    Subroutine write_nad_file(nd,ne,ranges_in,models,data)

    implicit none

    integer           nd,ne
    integer           nhu,nh,nhmax,nr,ns,ns1,i,n

    real(kind=4)      models(nd*ne)
    real(kind=4)      data(ne)
    real(kind=4)      ranges_in(2*nd)
    real(kind=4)      ranges(2,nd)
    real(kind=4)      scales(nd+1)


    character*256     fnme
    character*256     fnme_out

    parameter         (nhmax= 10000)
    character         header(nhmax)

    ! transform scales and ranges
    scales(1) = -1.0
    do i=2,(nd+1)
        scales(i)=1.0
        write(*,*)'scales(',i,') = ',scales(i)
    end do

    n=1
    do i=1,nd
        ranges(1,i) = ranges_in(n)
        ranges(2,i) = ranges_in(n+1)
        write(*,*)'ranges(1,', i ,') = ', ranges(1,i)
        write(*,*)'ranges(2,', i ,') = ', ranges(2,i)
        n=n+2
    end do



    fnme = 'junk'

    nr  = 0
    ns  = 0
    ns1 = 0
    nhu = 0
    nh  = 0


    call NA_header(15,fnme,header,nhmax,nh,nd,ranges,scales,ns1,ns,nr,nhu)

!   write direct access nad file
    write(*,*)
    write(*,*)' Writing direct access file...'
    write(*,*)
    fnme_out = 'ensemble.nad'
    call write_nad(10,fnme_out,nd,ne,nh,nhu,header,models,data)


    return
    end

!
! ---------------------------------------------------------------------
!
!       write_nad - write a direct access file in
!           multi-record NAD format
!
!       Input:
!             lu                : logical unit of file
!             fnme              : filename
!             nhmax             : maximum size of array header
!             ndmax             : maximum size of array data
!             nemax             : maximum size of array models
!
!       Output:
!             nh                : length in bytes of file header
!             nhu               : length in bytes of user portion of header
!             nd                : dimension of parameter space
!             ne                : number of models in ensemble
!             header            : header character string of length nh (char)
!             data(nd)          : array of data values for each model (real(kind=4))
!             models(nd,ne)     : array of model values  (real(kind=4))
!
!       Comments:
!                The direct access NAD file format:
!
!                VARIABLE       TYPE            SIZE IN BYTES
!                nd             int             4
!                ne             int             4
!                nh             int             4
!                nhu            int             4
!                header         character       nh
!                models         real(kind=4)          4*nd*ne
!                data           real(kind=4)          4*nd
!                tail           real(kind=4)          4
!
!        In single record mode a direct access file of length
!                [4x(4+nd*ne+ne+1) + nh] bytes is produced.
!
!        In multi record mode a direct access file of length
!                [(ne+1)*(max(20+nh,4(nd+1))] bytes is produced.
!
!           Calls are made to subroutine read_da.
!
!                This routine assumes that direct access
!                files are opened with the record length specified
!                in bytes. This is the default for most machines
!                but not Dec machines. (Often a compiler option is
!                available on the DEC/compaq to use bytes rather than
!                4-byte words.
!
!                                       M. Sambridge, RSES, November 2001
!
! ---------------------------------------------------------------------
!
    Subroutine write_nad(lu,fnme,nd,ne,nh,nhu,header,models,data)

    implicit none

    integer           lu,nd,ne,nh,nhu,i,is1,is2,len1,lenh,len2,mul,num
    real(kind=4)      models(nd,ne)
    real(kind=4)      data(ne)
    character         header(nh)
    character*256     fnme


    ! write new multi-record format

    ! calculate length of header
    len1 = 4*5+nh
    len2 = 4*(nd+1)
    mul  = 1 + (len1-1)/len2
    ! write(*,*)mul
    lenh = mul*len2
    num = ne + mul
    is1 = num*len2
    is2 = 4*(5+nd*ne+ne)+nh

    write(*,*)' Number of models                         :',ne
    write(*,*)' Number of dimensions                     :',nd
    write(*,*)' Original header length in bytes          :',len1
    write(*,*)' Final header length in bytes             :',lenh
    write(*,*)' Direct access file record length         :',len2
    write(*,*)' Number of records                        :',num
    write(*,*)' Size of nad file in multi-record format  :',is1
    write(*,*)' Size of nad file in single record format :',is2

    ! write header
    open(lu,file=fnme,status='unknown',form='unformatted',access='direct',recl=lenh)
    write(lu,rec=1)-mul,nd,ne,nh,nhu,header
    close(lu)
    ! write models
    open(lu,file=fnme,status='unknown',form='unformatted',access='direct',recl=len2)
    do i=1,ne
       call wnad(lu,mul+i,nd,models(1,i),data(i))
    end do
    close(lu)


    return
    end
!
! ----------------------------------------------------------------------------
!   write a single model
! ----------------------------------------------------------------------------

    Subroutine wnad(lu,i,nd,models,data)

    implicit none

    integer       lu,i,nd
    real(kind=4)  models(nd)
    real(kind=4)  data

    write(lu,rec=i)models,data

    return
    end
!
! ----------------------------------------------------------------------------
!
!       NA_header - writes NA-specific information to NAD header.
!
!           This routine adds various NA-header info to
!           the header written by the user.
!
!                       M. Sambridge, June 1999
!
! ----------------------------------------------------------------------------
!
    Subroutine NA_header(lu,fnme,header,nh_max,nh,nd,range,scales,n1,n2,n3,nhu)

    implicit none

    integer           lu,nh_max,nh,nd,n1,n2,n3,nhu,rlen,nh_na,nh_tot,len
    real(kind=4)      range(2,nd)
    real(kind=4)      scales(nd+1)
!    character*(*)     header
    character         header(nh_max)
    character*256     fnme

!   calculate total header length
    rlen    = 3*nd + 4
    len     = 4*rlen+nh
    nhu     = nh
    nh_na   = 4*rlen
    nh_tot  = len
!   write(*,*)' nh_user = ',nhu
!   write(*,*)' nh_na   = ',nh_na
!   write(*,*)' nh_tot  = ',nh_tot

    if(nh_tot.gt.nh_max)then
       write(*,*)
       write(*,*)' Error - header array too small'
       write(*,*)
       write(*,*)'         current size = ',nh_max
       write(*,*)'        required size = ',nh_tot
       write(*,*)
       write(*,*)' Remedy - adjust nh_max in parameter file and recompile'
       write(*,*)
       stop
    end if

!   write out header information
    call write_header(lu,fnme,len,nd,nh,range,scales,n1,n2,n3,header)

    nh = nh_tot

!   read header information into character string
    call read_header(lu,fnme,nh,len,header)

    return
    end
!
! ----------------------------------------------------------------------------
!
!       read_header - converts header information into a character
!             string by writing it to a direct access file
!             and then reading it back as a character string
!
!       Calls no other routines.
!
!                       M. Sambridge, June 1999
!
! ----------------------------------------------------------------------------
!
    Subroutine read_header(lu,fnme,nh,len,header)

    implicit none

    integer           lu,nh,len
    character         header(nh)
    character*256     fnme

    open(lu,file=fnme,status='unknown',form='unformatted',access='direct',recl=len)
    read(lu,rec=1)header
    close(lu)

    return
    end
!
! ----------------------------------------------------------------------------
!
!       write_header - converts header information into a character
!              string by writing it to a direct access file
!              and then reading it back as a character string
!
!       Calls no other routines.
!
!                       M. Sambridge, June 1999
!
! ----------------------------------------------------------------------------
!
    Subroutine write_header(lu,fnme,len,nd,nh,range,scales,n1,n2,n3,header)

    implicit none

    integer           lu,len,nd,nh,n1,n2,n3
    real(kind=4)      range(2,nd)
    real(kind=4)      scales(nd+1)
    character         header(nh)
    character*256     fnme

    open(lu,file=fnme,status='unknown',form='unformatted',access='direct',recl=len)
    write(lu,rec=1)n1,n2,n3,range,scales,header
    close(lu)

    return
    end
