clear all
close all

numprocs = 8;

for k = 0:(numprocs-1)
    all(k+1).data =load(['models_all_ID' num2str(k) '.txt']);
    own(k+1).data =load(['models_own_ID' num2str(k) '.txt']);
end




% --- all ---
for k = 1:numprocs
    figure('name',['all' num2str(k)])
    %plot(all(k).data(:,2),all(k).data(:,3));
    hold on;
    scatter(all(k).data(:,2),all(k).data(:,3),20,log10(all(k).data(:,4)),'filled')
    colorbar;
    axis square;
end

% --- own ---
for k = 1:numprocs
    figure('name',['own' num2str(k)])
    plot(own(k).data(:,2),own(k).data(:,3));
    hold on;
    scatter(own(k).data(:,2),own(k).data(:,3),20,log10(own(k).data(:,4)),'filled')
    colorbar;
    axis square;
end