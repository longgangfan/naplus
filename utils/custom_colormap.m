function map = custom_colormap(fh,teiler,resolution)


ranges      = get(fh,'Clim');



mindat      = roundn(ranges(1),-1);
maxdat      = roundn(ranges(2),-1);
resolution  = roundn(resolution,0);

if gca ~= fh
    caxis([mindat maxdat]);
end

if (mod(maxdat,1) || mod(mindat,1))
    scale = 100;
else
    scale = 1;
end


x           =   linspace(mindat,maxdat,((maxdat-mindat)*scale*resolution)+1);
lox         =   length(x);
loxt        =   length(x(x>=teiler));
topc        =   loxt;
bottomc     =   lox-loxt;

% 2d ppd margin
map         =   [flipud(jet(topc));mygray(bottomc)];

% analytics  
%map         =   [flipud(jet(bottomc));(mygray(topc))];

%map         =   [flipud(jet(bottomc));(myblue(topc))];
%map         =   [flipud(jet(bottomc));(gray(topc))];
%map         =   [flipud(jet(bottomc));flipud(blue(topc))];
%map         =   [flipud(jet(bottomc));flipud(othercolor('Bu_10',topc))];

end

% blue colormap
function bmap = blue(n)
bmap        =   zeros(n,3);
bmap(:,3)   =   linspace(0,0.5,n);
end

function bmap = mygray(n)
bmap        =   linspace(0.3,1,n)'*ones(1,3);
end

function bmap = myblue(n)
bmap        =   linspace(0.5,1.0,n)'*ones(1,3);
end