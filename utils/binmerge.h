/*
 * binmerge.h
 *
 *  Created on: 19.12.2012
 *      Author: tobibaumann
 */

#ifndef BINMERGE_H_
#define BINMERGE_H_

// --- system include ---------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>
using std::cout;
using std::endl;

// --- function declarations --------------------------------------------------
void binmerge_main();

// --- macros -----------------------------------------------------------------
#define IS_LITTLE_ENDIAN ( (*(char*)&num) == 1 )




//--- template functions ------------------------------------------------------
template <class T>
void ByteSwap(T *buff,int n){
	int      i,j;
	T        tmp=0,*buff1 = (T*)buff;
	char    *ptr1,*ptr2 = (char*)&tmp;
    
	for (j=0; j<n; j++) {
		ptr1 = (char*)(buff1 + j);
		for (i=0; i<(int)sizeof(T); i++) {
			ptr2[i] = ptr1[sizeof(T)-1-i];
		}
		for (i=0; i<(int) sizeof(T); i++) {
			ptr1[i] = ptr2[i];
		}
	}
	return;
}
//-----------------------------------------------------------------------------
template <class T>
void read_array(T *array,int n,int *position,FILE *fileBIN){
	const int num=1;
	size_t readcnt;
	readcnt = fread(array,sizeof(T),n, fileBIN);
	*position += readcnt*sizeof(T);
	if(IS_LITTLE_ENDIAN){
		ByteSwap(array,n);
	}
	return;
}
//-----------------------------------------------------------------------------
template <class T>
void write_array(T *array,int n,FILE *fileBIN){
	const int num=1;
	if(IS_LITTLE_ENDIAN){
		ByteSwap(array,n);
	}
	fwrite(array,sizeof(T),n,fileBIN);
	return;
}
//-----------------------------------------------------------------------------
template <class T>
void print_array(T *array, int length){
	for (int n=0; n<length; n++)
		cout << array[n] << " ";
	cout << endl;
}




#endif /* BINMERGE_H_ */
